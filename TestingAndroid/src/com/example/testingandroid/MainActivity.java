package com.example.testingandroid;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ImageView imageViewRound = (ImageView)findViewById(R.id.imageView_round);
		Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.dhanya);
		imageViewRound.setImageBitmap(icon);
		Login login = new Login();
		login.setId(5);
		login.setName("utam");
		Intent intent = new Intent(this	, Testing.class);
		intent.putExtra("name", login);
		startActivity(intent);
	//	new Save2Jboss().execute(login);
		
		/*TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);

        String imsiSIM1 = telephonyInfo.getImsiSIM1();
        String imsiSIM2 = telephonyInfo.getImsiSIM2();

        boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
        boolean isSIM2Ready = telephonyInfo.isSIM2Ready();

        boolean isDualSIM = telephonyInfo.isDualSIM();*/
    }
		


	class Save2Jboss extends AsyncTask<Login, Void, String> {

		@Override
		protected String doInBackground(Login... params) {
			String responseString = null;

			int sc = 0;
			try {
				Login p = params[0];
				HttpClient client = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost("http://20.20.0.103:8080/JAXRS-RESTEasy-0.0.1-SNAPSHOT/rest/getu/savebit");
				httpPost.setHeader("content-type", "application/json");
				httpPost.setHeader("Accept", "application/json");
				com.google.gson.Gson gson = new com.google.gson.Gson();
				String string = gson.toJson(p);
				StringEntity entity = new StringEntity(gson.toJson(p)
						.toString());
				entity.setContentType("application/json");
				httpPost.setEntity(entity);
				HttpResponse response = client.execute(httpPost);
				sc = response.getStatusLine().getStatusCode();
		//		HttpEntity entity11 = response.getEntity();
		//		responseString = EntityUtils.toString(entity11);
			} catch (Exception e) {
				Log.e("Server Error", e.toString());
				e.printStackTrace();
			}
			return Integer.toString(sc);
		}

		@Override
		protected void onPostExecute(String result) {

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
