package com.database.handlers;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "register";

	// Contacts table name
	public static final String TABLE_CONTACTS = "registration";
	public static final String TABLE_ITEMLIST = "itemlist";
	public static final String TABLE_SPECIALORDER = "specialorder";
	// Contacts Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_OLD_MAIL = "oldmail";
	private static final String KEY_EMAILID = "emailid";
	private static final String KEY_PWD = "password";
	private static final String KEY_FNAME = "fname";
	private static final String KEY_MNAME = "mname";
	private static final String KEY_LNAME = "lname";
	private static final String KEY_PH_NO = "phone_number";
	private static final String KEY_HOME_NO = "home_number";
	private static final String KEY_ADDRESS = "address";
	private static final String KEY_ZIP = "zip";
	// ItemList Table column Names
	public static final String KEY_IID = "iid";
	public static final String KEY_ITEMID = "itemid";
	public static final String KEY_ITEMCODE = "itemcode";
	public static final String KEY_ITEMNAME = "itemname";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		// create contacts table
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_OLD_MAIL + " TEXT, "
				+ KEY_EMAILID + " TEXT," + KEY_PWD + " TEXT," + KEY_FNAME
				+ " TEXT," + KEY_MNAME + " TEXT," + KEY_LNAME + " TEXT,"
				+ KEY_PH_NO + " TEXT," + KEY_HOME_NO + " TEXT," + KEY_ADDRESS
				+ " TEXT," + KEY_ZIP + " TEXT" + ")";
		// create itemlist table
		/*
		 * String CREATE_ITEMLIST_TABLE="CREATE TABLE " + TABLE_ITEMLIST +
		 * "("+KEY_IID+" INTEGER PRIMARY KEY,"+KEY_ITEMID+" TEXT,"
		 * +KEY_ITEMCODE+" TEXT,"+KEY_ITEMNAME+" TEXT"+")";
		 */

		String CREATE_ITEMLIST_TABLE = "CREATE TABLE " + TABLE_ITEMLIST + "("
				+ KEY_IID + " INTEGER PRIMARY KEY," + KEY_ITEMID + " TEXT,"
				+ KEY_ITEMNAME + " TEXT," + KEY_EMAILID + " TEXT" + ")";

		String CREATE_SPECIALORDER_TABLE = "CREATE TABLE " + TABLE_SPECIALORDER
				+ "(" + KEY_IID + " INTEGER PRIMARY KEY," + KEY_ITEMID
				+ " TEXT," + KEY_ITEMNAME + " TEXT," + KEY_EMAILID + " TEXT"
				+ ")";

		db.execSQL(CREATE_CONTACTS_TABLE);
		db.execSQL(CREATE_ITEMLIST_TABLE);
		db.execSQL(CREATE_SPECIALORDER_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMLIST);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SPECIALORDER);

		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations on Contacts table
	 */

	// Adding new contact
	public void addContact(Contact contact) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_OLD_MAIL, contact.getOldmail());
		values.put(KEY_EMAILID, contact.get_emailid());
		values.put(KEY_PWD, contact.getPassword());
		values.put(KEY_FNAME, contact.get_fname()); // Contact Name
		values.put(KEY_MNAME, contact.get_mname());
		values.put(KEY_LNAME, contact.get_lname());
		values.put(KEY_PH_NO, contact.get_phone_number()); // Contact Phone
		values.put(KEY_HOME_NO, contact.getHome_phone_number());
		values.put(KEY_ADDRESS, contact.getAddress());
		values.put(KEY_ZIP, contact.getZip());

		// Inserting Row
		db.insert(TABLE_CONTACTS, null, values);
		db.close(); // Closing database connection
	}

	// Getting single contact
	Contact getContact(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
				KEY_EMAILID, KEY_PWD, KEY_FNAME, KEY_MNAME, KEY_LNAME,
				KEY_PH_NO, KEY_HOME_NO, KEY_ADDRESS, KEY_ZIP }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		// if (cursor != null)
		cursor.moveToFirst();

		Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2), cursor.getString(3),
				cursor.getString(4), cursor.getString(5), cursor.getString(6),
				cursor.getString(7), cursor.getString(8), cursor.getString(9));
		// return contact
		return contact;
	}

	// Getting All Contacts
	public List<Contact> getAllContacts(String emailid) {
		List<Contact> contactList = new ArrayList<Contact>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "
				+ KEY_EMAILID + "='" + emailid + "'";
		// String selectQuery =
		// "SELECT  * FROM registration WHERE emailid='lakshmi@gmail.com'";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Contact contact = new Contact();
				contact.setID(Integer.parseInt(cursor.getString(0)));
				contact.set_emailid(cursor.getString(2));
				contact.setPassword(cursor.getString(3));
				contact.set_fname(cursor.getString(4));
				contact.set_mname(cursor.getString(5));
				contact.set_lname(cursor.getString(6));
				contact.set_phone_number(cursor.getString(7));
				contact.setHome_phone_number(cursor.getString(8));
				contact.setAddress(cursor.getString(9));
				contact.setZip(cursor.getString(10));
				// Adding contact to list
				contactList.add(contact);
			} while (cursor.moveToNext());
		}

		// return contact list
		return contactList;
	}

	// Updating single contact
	public int updateContact(Contact contact) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_OLD_MAIL, contact.getOldmail());
		values.put(KEY_EMAILID, contact.get_emailid());
		values.put(KEY_PWD, contact.getPassword());
		values.put(KEY_FNAME, contact.get_fname()); // Contact Name
		values.put(KEY_MNAME, contact.get_mname());
		values.put(KEY_LNAME, contact.get_lname());
		values.put(KEY_PH_NO, contact.get_phone_number()); // Contact Phone
		values.put(KEY_HOME_NO, contact.getHome_phone_number());
		values.put(KEY_ADDRESS, contact.getAddress());
		values.put(KEY_ZIP, contact.getZip());

		// updating row
		return db.update(TABLE_CONTACTS, values, KEY_OLD_MAIL + " = ?",
				new String[] { String.valueOf(contact.getOldmail()) });
	}

	// Deleting single contact
	public void deleteContact(Contact contact) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
				new String[] { String.valueOf(contact.getID()) });
		db.close();
	}

	// Getting contacts Count
	public int getContactsCount() {
		String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}// end operations on conatcts table

	// start CRUD operations on ItemList Table
	// Adding new contact
	public long addItemList(ItemList itemlist) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ITEMID, itemlist.getItemid());
		// values.put(KEY_ITEMCODE,itemlist.getCode());
		values.put(KEY_ITEMNAME, itemlist.getItemname());
		values.put(KEY_EMAILID, itemlist.getEmailid());
		// Inserting Row
		return db.insert(TABLE_ITEMLIST, null, values);
		// db.close(); // Closing database connection
	}

	// get one row of ItemList
	/*
	 * ItemList getItemList(int id) { SQLiteDatabase db =
	 * this.getReadableDatabase();
	 * 
	 * Cursor cursor = db.query(TABLE_ITEMLIST, new String[] {
	 * KEY_IID,KEY_ITEMID,KEY_ITEMCODE, KEY_ITEMNAME }, KEY_IID + "=?", new
	 * String[] { String.valueOf(id) }, null, null, null, null); //if (cursor !=
	 * null) cursor.moveToFirst();
	 * 
	 * ItemList contact = new ItemList(Integer.parseInt(cursor.getString(0)),
	 * cursor.getString(1), cursor.getString(2),cursor.getString(3)); // return
	 * contact return contact; }
	 */
	// Getting All Items
	public List<ItemList> getAllItems(String email) {
		List<ItemList> itemList = new ArrayList<ItemList>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_ITEMLIST + " WHERE "
				+ KEY_EMAILID + "='" + email + "' ORDER BY " + KEY_ITEMNAME;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// Cursor cursor=db.query(selectQuery, new
		// String[]{KEY_ITEMID,KEY_ITEMCODE,KEY_ITEMNAME},null, null, null,
		// null, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ItemList itemlist = new ItemList();
				itemlist.set_id(Integer.parseInt(cursor.getString(0)));
				itemlist.setItemid(cursor.getString(1));
				// itemlist.setCode(cursor.getString(2));
				itemlist.setItemname(cursor.getString(2));

				// Adding contact to list
				itemList.add(itemlist);
			} while (cursor.moveToNext());
		}
		return itemList;
	}

	// Updating single contact
	public int updateItem(ItemList contact) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ITEMID, contact.getItemid());
		// values.put(KEY_ITEMCODE,contact.getCode());
		values.put(KEY_ITEMNAME, contact.getItemname()); // Contact Name

		// updating row
		return db.update(TABLE_ITEMLIST, values, KEY_IID + " = ?",
				new String[] { String.valueOf(contact.get_id()) });
	}

	// Deleting single contact
	public Boolean deleteItem(ItemList contact) {
		SQLiteDatabase db = this.getWritableDatabase();

		/*return db.delete(TABLE_ITEMLIST, KEY_ITEMNAME + " = ?",
				new String[] {contact.getItemname()});*/
		return db.delete(TABLE_ITEMLIST, KEY_IID + "=" + contact.get_id(), null) > 0;

	}

	// Getting contacts Count
	public int getItemsCount() {
		String countQuery = "SELECT  * FROM " + TABLE_ITEMLIST;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}// end operations on conatcts table

	public boolean CheckIsDataAlreadyInDBorNot(String email, String id,
			String itemname) {
		SQLiteDatabase sqldb = this.getReadableDatabase();
		String Query = "Select * from " + TABLE_ITEMLIST + " where "
				+ KEY_EMAILID + "='" + email + "' AND " + KEY_ITEMNAME + "='"
				+ itemname + "'";
		Cursor cursor = sqldb.rawQuery(Query, null);
		if (cursor.getCount() <= 0) {
			return false;
		}
		return true;
	}

	// end items table operations
	public long addSItemList(SpeialOrder itemlist) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ITEMID, itemlist.getItemid());
		// values.put(KEY_ITEMCODE,itemlist.getCode());
		values.put(KEY_ITEMNAME, itemlist.getItemname());
		values.put(KEY_EMAILID, itemlist.getEmail());

		// Inserting Row
		return db.insert(TABLE_SPECIALORDER, null, values);
		// db.close(); // Closing database connection
	}

	// get one row of ItemList
	/*
	 * ItemList getItemList(int id) { SQLiteDatabase db =
	 * this.getReadableDatabase();
	 * 
	 * Cursor cursor = db.query(TABLE_ITEMLIST, new String[] {
	 * KEY_IID,KEY_ITEMID,KEY_ITEMCODE, KEY_ITEMNAME }, KEY_IID + "=?", new
	 * String[] { String.valueOf(id) }, null, null, null, null); //if (cursor !=
	 * null) cursor.moveToFirst();
	 * 
	 * ItemList contact = new ItemList(Integer.parseInt(cursor.getString(0)),
	 * cursor.getString(1), cursor.getString(2),cursor.getString(3)); // return
	 * contact return contact; }
	 */
	// Getting All Items
	public List<SpeialOrder> getAllSItems(String email) {
		List<SpeialOrder> itemList = new ArrayList<SpeialOrder>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_SPECIALORDER + " WHERE "
				+ KEY_EMAILID + "='" + email + "' ORDER BY " + KEY_ITEMNAME;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// Cursor cursor=db.query(selectQuery, new
		// String[]{KEY_ITEMID,KEY_ITEMCODE,KEY_ITEMNAME},null, null, null,
		// null, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				SpeialOrder itemlist = new SpeialOrder();
				itemlist.set_id(Integer.parseInt(cursor.getString(0)));
				itemlist.setItemid(cursor.getString(1));
				// itemlist.setCode(cursor.getString(2));
				itemlist.setItemname(cursor.getString(2));

				// Adding contact to list
				itemList.add(itemlist);
			} while (cursor.moveToNext());
		}

		// return contact list
		return itemList;
	}

	// Updating single contact
	public int updateSItem(SpeialOrder contact) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ITEMID, contact.getItemid());
		// values.put(KEY_ITEMCODE,contact.getCode());
		values.put(KEY_ITEMNAME, contact.getItemname()); // Contact Name

		// updating row
		return db.update(TABLE_SPECIALORDER, values, KEY_IID + " = ?",
				new String[] { String.valueOf(contact.get_id()) });
	}

	// Deleting single contact
	public int deleteSItem(SpeialOrder contact) {
		SQLiteDatabase db = this.getWritableDatabase();

		return db.delete(TABLE_SPECIALORDER, KEY_ITEMNAME + " = ?",
				new String[] { String.valueOf(contact.getItemname()) });

	}

	// Getting contacts Count
	public int getSItemsCount() {
		String countQuery = "SELECT  * FROM " + TABLE_SPECIALORDER;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}// end operations on conatcts table

	public boolean CheckIsItemAlreadyInDBorNot(String email, String itemname) {
		SQLiteDatabase sqldb = this.getReadableDatabase();
		String Query = "Select * from " + TABLE_SPECIALORDER + " where "
				+ KEY_EMAILID + "='" + email + "' AND " + KEY_ITEMNAME + "='"
				+ itemname + "'";
		Cursor cursor = sqldb.rawQuery(Query, null);
		if (cursor.getCount() <= 0) {
			return false;
		}
		return true;
	}
}
