package com.database.handlers;

public class Contact {
	
	//private variables
	int _id;
	String _emailid;
	String password;
	String _fname;
	String _mname;
	String _lname;
	String _phone_number;
	String home_phone_number;
	String address;
	String zip;
	String oldmail;
	
	// Empty constructor
	public Contact(){
		
	}
	// constructor
	public Contact(int id,String emailid,String password, String fname,String mname,String lname,
			String _phone_number,String home_phone_number,String address,String zipcode){
		this._id = id;
		this._emailid=emailid;
		this.password=password;
		this._fname = fname;
		this._mname = mname;
		this._lname = lname;		
		this._phone_number = _phone_number;
		this.home_phone_number = home_phone_number;
		this.address=address;
		this.zip=zipcode;
	}
	public Contact(String oldmail,String emailid,String password, String fname,String mname,String lname,
			String _phone_number,String home_phone_number,String address,String zipcode){
		this.oldmail =oldmail;
		this._emailid=emailid;
		this.password=password;
		this._fname = fname;
		this._mname = mname;
		this._lname = lname;		
		this._phone_number = _phone_number;
		this.home_phone_number = home_phone_number;
		this.address=address;
		this.zip=zipcode;
	}
	
	public String getOldmail() {
		return oldmail;
	}
	public void setOldmail(String oldmail) {
		this.oldmail = oldmail;
	}
	// constructor
	public Contact(String emailid,String password, String fname,String mname,String lname,
			String _phone_number,String home_phone_number,String address,String zipcode){
		this._emailid=emailid;
		this.password=password;
		this._fname = fname;
		this._mname = mname;
		this._lname = lname;		
		this._phone_number = _phone_number;
		this.home_phone_number = home_phone_number;
		this.address=address;
		this.zip=zipcode;
	}
	// getting ID
	public int getID(){
		return this._id;
	}
	
	// setting id
	public void setID(int id){
		this._id = id;
	}
	public String get_emailid() {
		return _emailid;
	}
	public void set_emailid(String _emailid) {
		this._emailid = _emailid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String get_fname() {
		return _fname;
	}
	public void set_fname(String _fname) {
		this._fname = _fname;
	}
	public String get_mname() {
		return _mname;
	}
	public void set_mname(String _mname) {
		this._mname = _mname;
	}
	public String get_lname() {
		return _lname;
	}
	public void set_lname(String _lname) {
		this._lname = _lname;
	}
	public String get_phone_number() {
		return _phone_number;
	}
	public void set_phone_number(String _phone_number) {
		this._phone_number = _phone_number;
	}
	public String getHome_phone_number() {
		return home_phone_number;
	}
	public void setHome_phone_number(String home_phone_number) {
		this.home_phone_number = home_phone_number;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	
}
