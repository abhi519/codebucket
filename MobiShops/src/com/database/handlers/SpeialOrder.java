package com.database.handlers;

public class SpeialOrder {
	int _id;
	String itemid;
	String code;
	String itemname;
	String email;

	public SpeialOrder() {

	}

	// constructor
	public SpeialOrder(int _id, String itemid, String code, String itemname) {
		this._id = _id;
		this.itemid = itemid;
		this.code = code;
		this.itemname = itemname;

	}

	// constructor
	public SpeialOrder(String email, String itemid, String itemname) {
		this.itemid = itemid;
		this.code = code;
		this.email = email;
		this.itemname = itemname;

	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getItemid() {
		return itemid;
	}

	public void setItemid(String itemid) {
		this.itemid = itemid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
