package com.database.handlers;

public class ItemList {
	int _id;
	String itemid;
	String code;
	String itemname;
	String itemwgt;
	String emailid;

	public ItemList() {

	}

	// constructor
	public ItemList(int _id, String itemid, String itemname,String email) {
		this._id = _id;
		this.itemid = itemid;
		this.itemname = itemname;
		this.emailid = email;

	}

	// constructor
	public ItemList(String emailid, String itemid, String itemname,
			String itemwgt) {
		this.itemid = itemid;
		this.code = code;
		this.emailid = emailid;
		this.itemname = itemname;
		this.itemwgt = itemwgt;

	}

	public String getItemwgt() {
		return itemwgt;
	}

	public void setItemwgt(String itemwgt) {
		this.itemwgt = itemwgt;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getItemid() {
		return itemid;
	}

	public void setItemid(String itemid) {
		this.itemid = itemid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

}
