package com.compare.mobishops;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.database.handlers.Contact;
import com.database.handlers.DatabaseHandler;
import com.example.mobishops.R;
import com.example.mobishops.SessionStore;

public class ShopatStore extends Activity implements OnClickListener {
	ProgressDialog pDialog;
	// private static final String SEND_ORDER_URL =
	// "http://www.sesm.test1mcis.com/cart.php";
	private static final String SEND_ORDER_URL = "http://www.sesm.test1mcis.com/order_confirm.php";
	HttpResponse response;
	HttpEntity resEntity;
	String response_str;
	String shop_id;
	Button shopatstore, pickup, homedelivery;
	ImageView calendar, calpickup;
	ArrayList<HashMap<String, String>> shopbyproduct = new ArrayList<HashMap<String, String>>();
	String Shoppingoption;
	DatabaseHandler dbHandler;
	String cellnumber, address;
	static final int DATE_DIALOG_ID = 999;
	static final int TIME_DIALOG_ID = 1;
	private int year;
	private int month, mHour, mMinute;
	private int day, hourSelected, minuteSelected;
	String date, time, email;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shopatstore);
		email = SessionStore.getString(getApplicationContext(),
				SessionStore.EMAIL, "");
//		List<Contact> contactlist = dbHandler.getAllContacts(email);
//		for (Contact cn : contactlist) {
//			cellnumber = cn.get_phone_number();
//			address = cn.getAddress();
//			Log.d("cellnumber", "" + cellnumber);
//			Log.d("address", "" + address);
//		}
		shopatstore = (Button) findViewById(R.id.bshop_store);
		pickup = (Button) findViewById(R.id.bpick_up);
		homedelivery = (Button) findViewById(R.id.bhome_delivery);
		calpickup = (ImageView) findViewById(R.id.imagepickup);
		calpickup.setOnClickListener(this);

		calendar = (ImageView) findViewById(R.id.imagehome);
		calendar.setOnClickListener(this);
		shopatstore.setOnClickListener(this);
		pickup.setOnClickListener(this);
		homedelivery.setOnClickListener(this);

		Intent b = getIntent();
		shopbyproduct = (ArrayList<HashMap<String, String>>) b
				.getSerializableExtra("shopbyproductarray");
		shop_id = b.getStringExtra("shopid");
		Log.d("shopid", "" + shop_id);
		Log.d("sdkg", "" + shopbyproduct);

	}

	class ComparOrder extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(ShopatStore.this);
			pDialog.setMessage("Loading Data...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(SEND_ORDER_URL);

			try {
				// Add your data

				ArrayList<NameValuePair> valueparams = new ArrayList<NameValuePair>();
				// Log.d("email",email);
				valueparams.add(new BasicNameValuePair("email_address", email));
				valueparams.add(new BasicNameValuePair("shop_id", String
						.valueOf(shop_id)));
				valueparams.add(new BasicNameValuePair("shopping_option",
						Shoppingoption));

				JSONArray json = new JSONArray(shopbyproduct);
				for (int i = 0; i < json.length(); i++) {
					try {
						JSONObject objJson = json.getJSONObject(i);
						String prodid = objJson.getString("pid");
						String prodqty = objJson.getString("prodqty");
						Log.d("string array", "" + objJson.getString("pid"));
						valueparams.add(new BasicNameValuePair(
								"prod_quantities[" + prodid + "]", prodqty));
						Log.d("prod_quantities[" + prodid + "]", prodqty);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				httppost.setEntity(new UrlEncodedFormEntity(valueparams));
				// Execute HTTP Post Request
				response = httpclient.execute(httppost);
				resEntity = response.getEntity();
				response_str = EntityUtils.toString(resEntity);
				if (resEntity != null) {
					Log.i("RESPONSE", response_str);
				}
			}

			catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return null;

		}

		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			Toast.makeText(getApplicationContext(), "shopatstore", Toast.LENGTH_SHORT).show();

		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.bshop_store:
			Shoppingoption = "S";
			new ComparOrder().execute();
			break;
		case R.id.bpick_up:
			Shoppingoption = "P";
			new PickupOder().execute();
			break;
		case R.id.bhome_delivery:
			Shoppingoption = "H";
			new HomeDelivery().execute();
			break;
		case R.id.imagehome:
			showDialog(DATE_DIALOG_ID);
			showDialog(TIME_DIALOG_ID);
			break;
		case R.id.imagepickup:
			showDialog(DATE_DIALOG_ID);
			showDialog(TIME_DIALOG_ID);
			break;

		default:
			break;
		}
	}

	class PickupOder extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(ShopatStore.this);
			pDialog.setMessage("Loading Data...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(SEND_ORDER_URL);

			try {
				// Add your data

				ArrayList<NameValuePair> valueparams = new ArrayList<NameValuePair>();
				// Log.d("email",email);
				valueparams.add(new BasicNameValuePair("email_address", email));
				valueparams.add(new BasicNameValuePair("shop_id", String
						.valueOf(shop_id)));
				valueparams.add(new BasicNameValuePair("shopping_option",
						Shoppingoption));
				valueparams
						.add(new BasicNameValuePair("time", date + "" + time));
				JSONArray json = new JSONArray(shopbyproduct);
				for (int i = 0; i < json.length(); i++) {
					try {
						JSONObject objJson = json.getJSONObject(i);
						String prodid = objJson.getString("pid");
						String prodqty = objJson.getString("prodqty");
						Log.d("string array", "" + objJson.getString("pid"));
						valueparams.add(new BasicNameValuePair(
								"prod_quantities[" + prodid + "]", prodqty));
						Log.d("prod_quantities[" + prodid + "]", prodqty);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				httppost.setEntity(new UrlEncodedFormEntity(valueparams));
				// Execute HTTP Post Request
				response = httpclient.execute(httppost);
				resEntity = response.getEntity();
				response_str = EntityUtils.toString(resEntity);
				if (resEntity != null) {
					Log.i("RESPONSE", response_str);
				}
			}

			catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return null;

		}

		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			Toast.makeText(getApplicationContext(), "PIckup", Toast.LENGTH_SHORT).show();

		}
	}

	class HomeDelivery extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(ShopatStore.this);
			pDialog.setMessage("Loading Data...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(SEND_ORDER_URL);

			try {
				// Add your data

				ArrayList<NameValuePair> valueparams = new ArrayList<NameValuePair>();
				// Log.d("email",email);
				valueparams.add(new BasicNameValuePair("email_address", email));
				valueparams.add(new BasicNameValuePair("shop_id", String
						.valueOf(shop_id)));
				valueparams.add(new BasicNameValuePair("shopping_option",
						Shoppingoption));
				valueparams
						.add(new BasicNameValuePair("time", date + "" + time));
				Log.d("", "" + date + "" + time);
				valueparams.add(new BasicNameValuePair("address", "kondapur"));
				valueparams.add(new BasicNameValuePair("cell_number",
						"9000301620"));

				JSONArray json = new JSONArray(shopbyproduct);
				for (int i = 0; i < json.length(); i++) {
					try {
						JSONObject objJson = json.getJSONObject(i);
						String prodid = objJson.getString("pid");
						String prodqty = objJson.getString("prodqty");
						Log.d("string array", "" + objJson.getString("pid"));
						valueparams.add(new BasicNameValuePair(
								"prod_quantities[" + prodid + "]", prodqty));
						Log.d("prod_quantities[" + prodid + "]", prodqty);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				httppost.setEntity(new UrlEncodedFormEntity(valueparams));
				// Execute HTTP Post Request
				response = httpclient.execute(httppost);
				resEntity = response.getEntity();
				response_str = EntityUtils.toString(resEntity);
				if (resEntity != null) {
					Log.i("RESPONSE", response_str);
				}
			}

			catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return null;

		}

		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			Toast.makeText(getApplicationContext(), "HomeDelivery", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// set date picker as current date
			return new DatePickerDialog(this, datePickerListener, year, month,
					day);
		case TIME_DIALOG_ID:
			return new TimePickerDialog(this, mTimeSetListener, mHour, mMinute,
					false);

		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// set selected date into textview
			// tvDisplayDate.setText(new StringBuilder().append(month + 1)
			// .append("-").append(day).append("-").append(year)
			// .append(" "));
			Toast.makeText(
					getApplicationContext(),
					new StringBuilder().append(month + 1).append("-")
							.append(day).append("-").append(year).append(" "),
					1000);
			StringBuilder datestring = new StringBuilder().append(month + 1)
					.append("-").append(day).append("-").append(year)
					.append(" ");
			date = datestring.toString();

			Log.d("date", "" + date);

			// set selected date into datepicker also
			// dpResult.init(year, month, day, null);

		}
	};
	// Register TimePickerDialog listener
	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
		// the callback received when the user "sets" the TimePickerDialog in
		// the dialog

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// TODO Auto-generated method stub
			hourSelected = hourOfDay;
			minuteSelected = minute;
			Toast.makeText(getApplicationContext(),
					"Time selected is:" + hourSelected + "-" + minuteSelected,
					Toast.LENGTH_LONG).show();
			time = hourSelected + ":" + minuteSelected;
			Log.d("time", "" + time);
		}
	};
}
