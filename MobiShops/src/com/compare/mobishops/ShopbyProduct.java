package com.compare.mobishops;

import java.util.ArrayList;
import java.util.HashMap;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.compare.mobishops.ShopbyPrice.ShopbyProductdetails;
import com.example.mobishops.CurrentOrderActivity;
import com.example.mobishops.R;

public class ShopbyProduct extends Activity {
	ArrayList<HashMap<String, Object>> eachproduct = new ArrayList<HashMap<String, Object>>();
	ArrayList<HashMap<String, String>> eachpriceList = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, Object>> something = new ArrayList<HashMap<String, Object>>();
	ArrayList<HashMap<String, String>> places = new ArrayList<HashMap<String, String>>();

	String array, shopid;
	Button addordelete, ord;
	ListView listView;
	NewsShopsdetails objectAdapter;
	ShopbyProductdetails objAdapter;

	// List<ProductInformation> arrayofprolist1;
	// ShopbyProductdetails objAdapter;
	// ArrayList<ProductInformation> Shop_Al = new
	// ArrayList<ProductInformation>();
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shopbyprice);

		Intent b = getIntent();
		places = (ArrayList<HashMap<String, String>>) b
				.getSerializableExtra("Array");
		shopid = b.getStringExtra("shopid");
		Log.d("shopid", "" + shopid);
		Log.d("sdkg", "" + places);

		listView = (ListView) findViewById(R.id.listview);
		addordelete = (Button) findViewById(R.id.butadd);
		addordelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ShopbyProduct.this,
						CurrentOrderActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivityForResult(i, 2);
			}
		});
		ord = (Button) findViewById(R.id.butdel);
		ord.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ShopbyProduct.this, ShopatStore.class);
				i.putExtra("shopbyproductarray", places);
				i.putExtra("shopid", shopid);
				startActivity(i);
			}
		});
		// arrayofprolist1 = new ArrayList<ProductInformation>();
		setAdapterToListview();
		// JsonDataComparision();

	}

	public void setAdapterToListview() {
		objectAdapter = new NewsShopsdetails(ShopbyProduct.this,
				R.layout.productsprice, places);
		listView.setAdapter(objectAdapter);
	}

	public class NewsShopsdetails extends BaseAdapter {

		Activity activity;
		private int row;
		ArrayList<HashMap<String, String>> places = new ArrayList<HashMap<String, String>>();

		public NewsShopsdetails(ShopbyProduct act, int productsprice,
				ArrayList<HashMap<String, String>> places) {
			super();
			// TODO Auto-generated constructor stub
			this.places = places;
			this.row = productsprice;
			this.activity = act;
		}

		public class ViewHolder {
			public TextView tvpname, tvpqyt, tvprice, tvdiscount, tvtotal,
					maps;
			public CheckBox check;
			public ImageView maps1;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return places.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;

			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(row, null);

				holder = new ViewHolder();
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			// objBean = items.get(position);
			// Log.d("position", "" + items.get(position).getId());

			holder.tvpname = (TextView) view.findViewById(R.id.pro_name);
			holder.tvpqyt = (TextView) view.findViewById(R.id.pro_qty);
			holder.tvprice = (TextView) view.findViewById(R.id.pro_price);
			holder.tvdiscount = (TextView) view.findViewById(R.id.pro_coupon);
			holder.tvtotal = (TextView) view.findViewById(R.id.pro_total);
			holder.tvpname.setText(places.get(position).get("prodname"));
			holder.tvpqyt.setText(places.get(position).get("prodqty"));
			holder.tvprice.setText(places.get(position).get("proprice"));
			holder.tvdiscount.setText(places.get(position).get("prodiscount"));
			holder.tvtotal.setText(places.get(position).get("prototal"));

			return view;
		}

	}

}