package com.compare.mobishops;

public class Products {
	private String price;
	private String quantity;
	private String selected_quantity;
	private String name;
	private String pname;
	private String in_stock;
	private String coupon_code;
	private String discount;
	private String discount_mode;
	private String coupon_status;
	private String final_price;
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getSelected_coupon() {
		return selected_coupon;
	}
	public void setSelected_coupon(String selected_coupon) {
		this.selected_coupon = selected_coupon;
	}
	public String getDiscounted_amt() {
		return discounted_amt;
	}
	public void setDiscounted_amt(String discounted_amt) {
		this.discounted_amt = discounted_amt;
	}
	private int product_id;
	private String selected_coupon;
	private String discounted_amt;

	public int getProduct_id() {
	return product_id;
}
public void setProduct_id(int product_id) {
	this.product_id = product_id;
}
public String getPrice() {
	return price;
}
public void setPrice(String price) {
	this.price = price;
}
public String getQuantity() {
	return quantity;
}
public void setQuantity(String quantity) {
	this.quantity = quantity;
}
public String getSelected_quantity() {
	return selected_quantity;
}
public void setSelected_quantity(String selected_quantity) {
	this.selected_quantity = selected_quantity;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getIn_stock() {
	return in_stock;
}
public void setIn_stock(String in_stock) {
	this.in_stock = in_stock;
}
public String getCoupon_code() {
	return coupon_code;
}
public void setCoupon_code(String coupon_code) {
	this.coupon_code = coupon_code;
}
public String getDiscount() {
	return discount;
}
public void setDiscount(String discount) {
	this.discount = discount;
}
public String getDiscount_mode() {
	return discount_mode;
}
public void setDiscount_mode(String discount_mode) {
	this.discount_mode = discount_mode;
}
public String getCoupon_status() {
	return coupon_status;
}
public void setCoupon_status(String coupon_status) {
	this.coupon_status = coupon_status;
}
public String getFinal_price() {
	return final_price;
}
public void setFinal_price(String final_price) {
	this.final_price = final_price;
}
	
}
