package com.compare.mobishops;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mobishops.R;

public class SampleShops extends Activity {
	String array;
	ArrayList<String> nofproducts;
	List<Shops> arrayOfList;
	ListView listView;
	private static final String ARRAY_NAME = "posts";
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String LATITUDE = "latitude";
	private static final String LONGITUDE = "longitude";
	private static final String TOTAL = "total";
	private static final String POSTSINF = "post_info";
	private static final String SELECTEDNAME = "post_name";
	private static final String SELECTEDQTY = "selected_qty";
	private static final String SELECTEDCOUPON = "selected_coupon";
	private static final String DBINF = "db_info";
	private static final String TAG_PRICE = "price";
	private static final String TAG_DISCOUNT = "discount";
	private static final String TAG_POSTID = "post_id";
	String distance, lati, strlat, strlong, shopid;
	GPSTracker gps;
	JSONArray contacts = null;
	double latitude1, longitude1;
	// Hashmap for ListView
	ArrayList<HashMap<String, String>> prodetails;
	ArrayList<HashMap<String, String>> prodetails1;
	ArrayList<HashMap<String, String>> passdetails;
	ArrayList<HashMap<String, String>> prodetails2;
	HashMap<String, String> details, details1, details2, dlist;
	NewsShopsdetails objectAdapter;
	private CheckBox listcheckbox = null;
	int listIndex = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mobi);
		Bundle b = getIntent().getExtras();
		if (b != null) {
			array = b.getString("Array");
			nofproducts = b.getStringArrayList("produtsno");
			Log.d("ppppp", "" + nofproducts);
		}
		prodetails = new ArrayList<HashMap<String, String>>();
		passdetails = new ArrayList<HashMap<String, String>>();
		prodetails2 = new ArrayList<HashMap<String, String>>();
		prodetails1 = new ArrayList<HashMap<String, String>>();

		listView = (ListView) findViewById(R.id.listview);

		gps = new GPSTracker(SampleShops.this);
		if (gps.canGetLocation()) {
			latitude1 = gps.getLatitude();
			longitude1 = gps.getLongitude();
			strlat = String.valueOf(latitude1);
			strlong = String.valueOf(longitude1);
			Log.i("sample location", strlat + "," + strlong);
			// show waiting screen
		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}

		JsonDataComparision();
		setAdapterToListview();

	}

	private void JsonDataComparision() {
		// TODO Auto-generated method stub
		try {
			JSONObject mainJson = new JSONObject(array);
			JSONArray jsonArray = mainJson.getJSONArray(ARRAY_NAME);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject objJson = jsonArray.getJSONObject(i);

				String id = objJson.getString(ID);
				String name = objJson.getString(NAME);
				String total = objJson.getString(TOTAL);
				String latitude = objJson.getString(LATITUDE);
				String longitude = objJson.getString(LONGITUDE);
				details = new HashMap<String, String>();
				details.put("id", id);
				details.put("name", name);
				details.put("total", total);
				details.put("latitude", latitude);
				details.put("longitude", longitude);
				Collection<String> values1 = details.values();
				values1.iterator();
				prodetails.add(details);
				// Log.d("prodetails",""+prodetails);
				String selectedname = null, selectedid = null, selectedqty = null, selectedcoupons = null, dbprice = null, dbdiscount = null;

				JSONArray postinfo = objJson.getJSONArray(POSTSINF);
				for (int j = 0; j < postinfo.length(); j++) {
					JSONObject object = postinfo.getJSONObject(j);
					details1 = new HashMap<String, String>();
					selectedname = object.getString(SELECTEDNAME);
					selectedqty = object.getString(SELECTEDQTY);
					selectedcoupons = object.getString(SELECTEDCOUPON);
					selectedid = object.getString(TAG_POSTID);
					details1.put("pid", selectedid);
					details1.put("prname", selectedname);
					details1.put("pqty", selectedqty);
					details1.put("pcoupon", selectedcoupons);
					Collection<String> values = details1.values();
					values.iterator();
					prodetails1.add(details1);
					Log.d("prodetails", "" + prodetails1);
				}

				JSONArray dbinfo = objJson.getJSONArray(DBINF);
				for (int k = 0; k < dbinfo.length(); k++) {
					JSONObject object1 = dbinfo.getJSONObject(k);
					details2 = new HashMap<String, String>();
					dbprice = object1.getString(TAG_PRICE);
					dbdiscount = object1.getString(TAG_DISCOUNT);
					details2.put("price", dbprice);
					details2.put("discount", dbdiscount);
					Collection<String> values2 = details2.values();
					values2.iterator();
					prodetails2.add(details2);
					Log.d("prodetails", "" + prodetails2);
				}

				// prodetails.add(details);

				Log.d("prodetails", "" + prodetails);

			}
		} catch (JSONException json) {
			json.printStackTrace();
		}
	}

	public void setAdapterToListview() {
		objectAdapter = new NewsShopsdetails(SampleShops.this,
				R.layout.mobilist, prodetails);
		listView.setAdapter(objectAdapter);
	}

	public class NewsShopsdetails extends BaseAdapter {
		int layout;
		ArrayList<HashMap<String, String>> prodetails;
		Activity sampleShops;

		public NewsShopsdetails(SampleShops sampleShops, int mobilist,
				ArrayList<HashMap<String, String>> prodetails) {
			// TODO Auto-generated constructor stub
			super();
			this.sampleShops = sampleShops;
			this.layout = mobilist;
			this.prodetails = prodetails;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return prodetails.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View view = convertView;
			final ViewHolder holder;

			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) sampleShops
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(layout, null);

				holder = new ViewHolder();
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			String latitude = prodetails.get(position).get("latitude");
			String longitude = prodetails.get(position).get("longitude");
			final Double dlat2 = Double.parseDouble(latitude);
			final Double dlongi2 = Double.parseDouble(longitude);

			double earthRadius = 3958.75;
			double dLat = Math.toRadians(dlat2 - latitude1);
			double dLng = Math.toRadians(dlongi2 - longitude1);
			double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
					+ Math.cos(Math.toRadians(latitude1))
					* Math.cos(Math.toRadians(dlat2)) * Math.sin(dLng / 2)
					* Math.sin(dLng / 2);
			double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			double dist = (earthRadius * c) / 1000;
			double dis = dist * 0.62137;
			String direction = new DecimalFormat("##.##").format(dis);

			holder.tvshop = (TextView) view.findViewById(R.id.tshop);
			holder.tvprice = (TextView) view.findViewById(R.id.tprice);
			holder.tvdistance = (TextView) view.findViewById(R.id.tdistance);

			holder.tvshop.setText(prodetails.get(position).get("name"));
			holder.tvprice.setText(Html.fromHtml("<u>" + "$"
					+ prodetails.get(position).get("total") + "<u>"));
			holder.tvdistance.setText(Html
					.fromHtml("<u>" + direction + " Mile"));

			holder.tvdistance.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String url = "http://maps.google.com/maps?saddr="
							+ latitude1 + "," + longitude1 + "&daddr=" + dlat2
							+ "," + dlongi2;
					Intent intent = new Intent(
							android.content.Intent.ACTION_VIEW, Uri.parse(url));
					intent.setClassName("com.google.android.apps.maps",
							"com.google.android.maps.MapsActivity");
					startActivity(intent);
				}
			});
			holder.check = (CheckBox) view.findViewById(R.id.checkBox1);
			holder.check.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					shopid = prodetails.get(position).get("id");
					View vMain = ((View) v.getParent());
					// uncheck previous checked button.
					if (listcheckbox != null)
						listcheckbox.setChecked(false);
					// assign to the variable the new one
					listcheckbox = (CheckBox) v;
					// find if the new one is checked or not, and set
					// "listIndex"
					if (listcheckbox.isChecked()) {
						listIndex = ((ViewGroup) vMain.getParent())
								.indexOfChild(vMain);

					} else {
						listcheckbox = null;
						listIndex = -1;
					}
				}
			});
			holder.tvprice.setTag(holder.check);
			holder.tvprice.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v.getTag();
					// shopid=prodetails.get(position).get("id");
					String pro_name = null, pro_price = null, pro_discount, pro_qty = null, pro_id = null;
					String pro_total1;
					if (shopid != null && shopid.equals(prodetails.get(position).get("id"))) {
						// if(prodetails.size()==prodetails1.size() &&
						// prodetails.size()==prodetails2.size() ){
						for (int d = 0; d < nofproducts.size(); d++) {
							pro_id = prodetails1.get(d).get("pid");
							pro_name = prodetails1.get(d).get("prname");
							pro_qty = prodetails1.get(d).get("pqty");
							pro_price = prodetails2.get(d).get("price");
							pro_discount = prodetails2.get(d).get("discount");
							double value1 = Double.parseDouble(prodetails2
									.get(d).get("price").toString());
							Double value2 = Double.parseDouble(prodetails1
									.get(d).get("pqty").toString());
							Double value3 = Double.parseDouble(prodetails2
									.get(d).get("discount").toString());
							pro_total1 = String.valueOf((value2)
									* (value1 - value3));
							//
							dlist = new HashMap<String, String>();
							dlist.put("pid", pro_id);
							dlist.put("prodname", pro_name);
							dlist.put("prodqty", pro_qty);
							dlist.put("proprice", pro_price);
							dlist.put("prodiscount", pro_discount);
							dlist.put("prototal", pro_total1);
							Collection<String> values = dlist.values();
							values.iterator();
							passdetails.add(dlist);
						}
						// }
					}
					if(cb!=null && cb.isChecked() == true){
					Intent i = new Intent(SampleShops.this, ShopbyProduct.class);
					i.putExtra("Array", passdetails);
					i.putExtra("shopid", shopid);
					Log.d("knknksn", "" + passdetails);
					startActivity(i);
					passdetails.clear();
					}
					else
						Toast.makeText(SampleShops.this, "Please select a store and then price in it", Toast.LENGTH_SHORT).show();

				}

			});

			return view;
		}

	}

	public class ViewHolder {
		public TextView tvshop, tvprice, tvdistance, tvlocation, tvAge, maps;
		public CheckBox check;
		public ImageView maps1;
	}
}
