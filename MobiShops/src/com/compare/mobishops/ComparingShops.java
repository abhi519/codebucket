package com.compare.mobishops;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mobishops.R;

public class ComparingShops extends Activity implements OnItemClickListener {
	String array;
	List<Shops> arrayOfList;
	ListView listView;
	private static final String ARRAY_NAME = "posts";
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String LATITUDE = "latitude";
	private static final String LONGITUDE = "longitude";
	private static final String TOTAL = "total";
	private static final String POSTSINF = "post_info";
	private static final String SELECTEDNAME = "post_name";
	private static final String SELECTEDQTY = "selected_qty";
	private static final String SELECTEDCOUPON = "selected_coupon";
	private static final String DBINF = "db_info";
	private static final String TAG_PRICE = "price";
	private static final String TAG_QUANTITY = "quantity";
	private static final String TAG_NAME = "pname";
	private static final String TAG_IN_STOCK = "in_stock";
	private static final String TAG_COUPON_CODE = "coupon_code";
	private static final String TAG_DISCOUNT = "discount";
	private static final String TAG_DISCOUNT_MODE = "discount_mode";
	private static final String TAG_COUPON_STATUS = "coupon_status";
	private static final String TAG_DISCOUNTED_AMT = "discounted_amt";

	ArrayList<HashMap<String, String>> detailsList = new ArrayList<HashMap<String, String>>();
	// ArrayList<HashMap<String, String>> eachpriceList = new
	// ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, Object>> something = new ArrayList<HashMap<String, Object>>();
	ArrayList<HashMap<String, String>> detailsList1 = new ArrayList<HashMap<String, String>>();
	NewsShopsdetails objectAdapter;
	String distance, lati, strlat, strlong;
	GPSTracker gps;
	String longi;
	EditText searchbox;
	ImageView im;
	ArrayList<Shops> Shop_Al = new ArrayList<Shops>();
	String ph;
	double latitude, longitude;
	private CheckBox listcheckbox = null;
	int listIndex = -1;
	ProgressDialog pDialog;
	HttpResponse response;
	HttpEntity resEntity;
	String response_str, shopid;
	// int shopid;
	HashMap<String, String> priceslist;
	HashMap<String, String> dlist;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mobi);

		Bundle b = getIntent().getExtras();
		if (b != null) {
			array = b.getString("Array");
			// Log.d("", "" + array);
		}

		listView = (ListView) findViewById(R.id.listview);
		listView.setOnItemClickListener(this);

		arrayOfList = new ArrayList<Shops>();
		JsonDataComparision();
	}

	public void JsonDataComparision() {
		try {
			JSONObject mainJson = new JSONObject(array);
			JSONArray jsonArray = mainJson.getJSONArray(ARRAY_NAME);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject objJson = jsonArray.getJSONObject(i);

				priceslist = new HashMap<String, String>();

				priceslist.put("sid", objJson.getString(ID));
				priceslist.put("sname", objJson.getString(NAME));
				priceslist.put("Total", objJson.getString(TOTAL));
				priceslist.put("lat", objJson.getString(LATITUDE));
				priceslist.put("long", objJson.getString(LONGITUDE));

				JSONArray json = objJson.getJSONArray(POSTSINF);
				for (int j = 0; j < json.length(); j++) {
					JSONObject object = json.getJSONObject(j);
					priceslist = new HashMap<String, String>();
					priceslist.put("pname", object.getString(SELECTEDNAME));
					priceslist.put("pqty", object.getString(SELECTEDQTY));
					priceslist.put("coupon", object.getString(SELECTEDCOUPON));

				}
				JSONArray jsondb = objJson.getJSONArray(DBINF);
				for (int k = 0; k < jsondb.length(); k++) {
					JSONObject objectdb = jsondb.getJSONObject(k);
					priceslist = new HashMap<String, String>();
					// HashMap<String, String> eachprice = new HashMap<String,
					// String>();
					String productname = objectdb.getString(TAG_NAME);
					String productprice = objectdb.getString(TAG_PRICE);
					String productquantity = objectdb.getString(TAG_QUANTITY);
					String stock = objectdb.getString(TAG_IN_STOCK);
					String couponcode = objectdb.getString(TAG_COUPON_CODE);
					String discount = objectdb.getString(TAG_DISCOUNT);
					String discountmode = objectdb.getString(TAG_DISCOUNT_MODE);
					String couponstatus = objectdb.getString(TAG_COUPON_STATUS);
					String discountedamt = objectdb
							.getString(TAG_DISCOUNTED_AMT);
					priceslist.put("productname", productname);
					priceslist.put("productprice", productprice);
					priceslist.put("productquantity", productquantity);
					priceslist.put("discountmode", discountmode);
					priceslist.put("discount", discount);
					priceslist.put("couponcode", couponcode);
					priceslist.put("stock", stock);
					priceslist.put("couponstatus", couponstatus);
					priceslist.put("discountedamt", discountedamt);
					Collection<String> values = priceslist.values();
					values.iterator();
					detailsList.add(priceslist);
					Log.d("detaillist", "" + detailsList);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Collections.sort(arrayOfList, new Comparator<Shops>() {

			@Override
			public int compare(Shops lhs, Shops rhs) {
				return (lhs.getId() - rhs.getId());
			}
		});
		setAdapterToListview();

	}

	public void setAdapterToListview() {
		objectAdapter = new NewsShopsdetails(ComparingShops.this,
				R.layout.mobilist, detailsList);
		listView.setAdapter(objectAdapter);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

	}

	public class NewsShopsdetails extends BaseAdapter {
		ListView lv;
		Activity activity;

		private int row;
		ArrayList<HashMap<String, String>> detailpriceList = new ArrayList<HashMap<String, String>>();

		public NewsShopsdetails(ComparingShops act, int mobilist,
				ArrayList<HashMap<String, String>> detailsList) {
			// TODO Auto-generated constructor stub
			this.activity = act;
			this.row = mobilist;
			this.detailpriceList = detailsList;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;

			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(row, null);

				holder = new ViewHolder();
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			priceslist = detailpriceList.get(position);

			// starting...distance calculation...

			String latti = detailpriceList.get(position).get("lat");
			String longii = detailpriceList.get(position).get("long");
			final Double dlat2 = Double.parseDouble(latti);
			final Double dlongi2 = Double.parseDouble(longii);

			double earthRadius = 3958.75;
			double dLat = Math.toRadians(dlat2 - latitude);
			double dLng = Math.toRadians(dlongi2 - latitude);
			double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
					+ Math.cos(Math.toRadians(latitude))
					* Math.cos(Math.toRadians(dlat2)) * Math.sin(dLng / 2)
					* Math.sin(dLng / 2);
			double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			double dist = (earthRadius * c) / 1000;
			double dis = dist * 0.62137;
			String direction = new DecimalFormat("##.##").format(dis);
			// ending......

			holder.tvshop = (TextView) view.findViewById(R.id.tshop);
			holder.tvprice = (TextView) view.findViewById(R.id.tprice);
			holder.tvdistance = (TextView) view.findViewById(R.id.tdistance);

			/*
			 * if (holder.tvshop != null && null != objBean.getName() &&
			 * objBean.getName().trim().length() > 0) {
			 * holder.tvshop.setText(Html.fromHtml(objBean.getName())); }
			 * 
			 * holder.tvprice.setText(Html.fromHtml("<u>" + "$" +
			 * (objBean.getTotal()) + "<u>"));
			 * 
			 * if (holder.tvdistance != null && objBean.getId() > 0) {
			 * holder.tvdistance.setText(Html.fromHtml("<u>" + direction +
			 * " Mile")); }
			 */
			holder.tvshop.setText(detailpriceList.get(position).get("sname"));
			holder.tvprice.setText(detailpriceList.get(position).get("Total"));
			holder.tvdistance.setText(direction);

			holder.tvdistance.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String url = "http://maps.google.com/maps?saddr="
							+ latitude + "," + longitude + "&daddr=" + dlat2
							+ "," + dlongi2;
					Intent intent = new Intent(
							android.content.Intent.ACTION_VIEW, Uri.parse(url));
					intent.setClassName("com.google.android.apps.maps",
							"com.google.android.maps.MapsActivity");
					startActivity(intent);
				}
			});

			holder.check = (CheckBox) view.findViewById(R.id.checkBox1);
			holder.check.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String shopname = detailpriceList.get(position)
							.get("sname");
					shopid = detailpriceList.get(position).get("sid");
					Log.d("Shopid", "" + shopid);
					View vMain = ((View) v.getParent());
					// uncheck previous checked button.
					if (listcheckbox != null)
						listcheckbox.setChecked(false);
					// assign to the variable the new one
					listcheckbox = (CheckBox) v;
					// find if the new one is checked or not, and set
					// "listIndex"
					if (listcheckbox.isChecked()) {
						listIndex = ((ViewGroup) vMain.getParent())
								.indexOfChild(vMain);
						Toast.makeText(
								getApplicationContext(),
								"shopname= " + shopname + "," + "shopid"
										+ shopid, 20).show();
					} else {
						listcheckbox = null;
						listIndex = -1;
					}
				}
			});

			holder.tvprice.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String pro_name = null, pro_price = null, pro_discount, pro_total1 = null, pro_qty = null;
					// Log.d("position", "" + items.get(position));
					if (shopid.equals(detailpriceList.get(position).get("sid"))) {
						for (int d = 0; d < detailpriceList.size(); d++) {
							pro_name = detailpriceList.get(position).get(
									"pname");
							pro_qty = detailpriceList.get(position).get("pqty");
							pro_price = detailpriceList.get(position).get(
									"productprice");
							pro_discount = detailpriceList.get(position).get(
									"productquantity");
							pro_total1 = detailpriceList.get(position).get(
									"Total");
							//
							dlist = new HashMap<String, String>();
							dlist.put("prodname", pro_name);
							dlist.put("prodqty", pro_qty);
							dlist.put("proprice", pro_price);
							dlist.put("prodiscount", pro_discount);
							dlist.put("prototal", pro_total1);
							Collection<String> values = dlist.values();
							values.iterator();
							detailsList1.add(dlist);
						}
					}
					Intent i = new Intent(ComparingShops.this,
							ShopbyProduct.class);
					i.putExtra("detailsList1", detailsList1);
					startActivity(i);
				}

			});

			return view;
		}

		public class ViewHolder {
			public TextView tvshop, tvprice, tvdistance, tvlocation, tvAge,
					maps;
			public CheckBox check;
			public ImageView maps1;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return detailpriceList.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}
	}

}
