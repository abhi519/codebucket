package com.compare.mobishops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import com.database.handlers.DatabaseHandler;
import com.database.handlers.ItemList;
import com.example.mobishops.CurrentOrderActivity;
import com.example.mobishops.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ShopbyPrice extends Activity {

	ListView listView;
	List<Products> arrayofprolist;
	ShopbyProductdetails objAdapter;
	ArrayList<Products> Shop_Al = new ArrayList<Products>();
	String shop_id;
	HttpResponse response;
	HttpEntity resEntity;
	String response_str;
	SharedPreferences preferences;
	DatabaseHandler dbHandler;
	HashMap<String, String> map;
	String quantities, coupons;
	ArrayList<String> myAListquantity = new ArrayList<String>();
	ArrayList<String> myAListcoupons = new ArrayList<String>();
	ArrayList<HashMap<String, String>> currentItems;
	int size;
	List<ItemList> list;
	ProgressDialog pDialog;
	Button addordelete;
	ArrayList<HashMap<String, String>> prices = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> eachprice = new ArrayList<HashMap<String, String>>();

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shopbyprice);
		
		 Bundle bundle = getIntent().getExtras();
		 if (bundle != null) {
		 prices = (ArrayList<HashMap<String, String>>) bundle
		 .getSerializable("selectedproduct");
		 eachprice = (ArrayList<HashMap<String, String>>) bundle
		 .getSerializable("eachprice");
		
		 Log.d("prices", "" + prices);
		 Log.d("eachprice", "" + eachprice);
		
		 }

		listView = (ListView) findViewById(R.id.listview);
		addordelete = (Button) findViewById(R.id.butadd);
		addordelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ShopbyPrice.this,
						CurrentOrderActivity.class);
						i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); 
						int requestCode =0;
						startActivityForResult(i, requestCode);
			}
		});
		// merge(prices, eachprice);
		arrayofprolist = new ArrayList<Products>();
		if (Utils.isNetworkAvailable(ShopbyPrice.this)) {
		} else {
			// showToast("No Network Connection!!!");
			Toast.makeText(getApplicationContext(), "No Network Connection!!!",
					Toast.LENGTH_LONG).show();
		}
	}

	public static ArrayList<HashMap<String, String>> merge(
			ArrayList<HashMap<String, String>> prices,
			ArrayList<HashMap<String, String>> eachprice) {

		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
		// now we assume that for each team there is one chance (valid?)
		for (Map<String, String> team : prices) {
			boolean success = false;
			HashMap<String, String> combined = new HashMap<String, String>();
			combined.putAll(team);

			String id = team.get("name");

			// now we have to find the "chance" map
			for (Map<String, String> chance : eachprice) {
				if (chance.get("pname").equals(id)) {
					combined.putAll(chance);
					boolean success1 = true;
					break;
				}
			}

			if (!success) {
				// there was no entry in chances map with this id!! -> handle
				// problem
			}
			result.add(combined);
		}
		return result;
	}

	// class MyTask extends AsyncTask<String, Void, String> {
	//
	// @Override
	// protected void onPreExecute() {
	// super.onPreExecute();
	//
	// pDialog = new ProgressDialog(ShopbyPrice.this);
	// /*
	// * pDialog.setMessage("Loading..."); pDialog.setCancelable(false);
	// * pDialog.show();
	// */
	// pDialog = ProgressDialog.show(ShopbyPrice.this, "101Mobishopping",
	// "Loading Shop's");
	// }
	//
	// @Override
	// protected String doInBackground(String... params) {
	// // TODO Auto-generated method stub
	// HttpClient httpclient = new DefaultHttpClient();
	// HttpPost httppost = new HttpPost(priceurl);
	//
	// try {
	// // Add your data
	//
	// ArrayList<NameValuePair> valueparams = new ArrayList<NameValuePair>();
	// // Log.d("email",email);
	// valueparams.add(new BasicNameValuePair("Shop_id", "2"));
	//
	// valueparams.add(new BasicNameValuePair("prod_quantities[1]",
	// "4"));
	// valueparams.add(new BasicNameValuePair("prod_coupons[1]",
	// "ASDFG"));
	//
	// httppost.setEntity(new UrlEncodedFormEntity(valueparams));
	//
	// // Execute HTTP Post Request
	//
	// response = httpclient.execute(httppost);
	// resEntity = response.getEntity();
	// response_str = EntityUtils.toString(resEntity);
	// if (resEntity != null) {
	// Log.i("RESPONSE", response_str);
	// }
	// }
	//
	// catch (ClientProtocolException e) {
	// // TODO Auto-generated catch block
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// }
	//
	// return null;
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	// super.onPostExecute(result);
	//
	// if (null != pDialog && pDialog.isShowing()) {
	// pDialog.dismiss();
	// }
	//
	// if (null == result || result.length() == 0) {
	// Toast.makeText(getApplicationContext(),
	// "No data found from web!!!", Toast.LENGTH_LONG).show();
	// ShopbyPrice.this.finish();
	// } else {
	//
	// try {
	// JSONObject mainJson = new JSONObject(response_str);
	// JSONArray jsonArray = mainJson.getJSONArray(TAG_POSTS);
	// for (int i = 0; i < jsonArray.length(); i++) {
	// JSONObject objJson = jsonArray.getJSONObject(i);
	//
	// Products objItem = new Products();
	// objItem.setProduct_id(objJson.getInt(TAG_PRODUCT_ID));
	// objItem.setPrice(objJson.getString(TAG_PRICE));
	// objItem.setCoupon_code(objJson
	// .getString(TAG_COUPON_CODE));
	// objItem.setCoupon_status(objJson
	// .getString(TAG_COUPON_STATUS));
	// objItem.setDiscount(objJson.getString(TAG_DISCOUNT));
	// objItem.setDiscount_mode(objJson
	// .getString(TAG_DISCOUNT_MODE));
	// objItem.setFinal_price(objJson
	// .getString(TAG_FINAL_PRICE));
	// objItem.setIn_stock(objJson.getString(TAG_IN_STOCK));
	// objItem.setName(objJson.getString(TAG_NAME));
	// objItem.setQuantity(objJson.getString(TAG_QUANTITY));
	// objItem.setSelected_quantity(objJson
	// .getString(TAG_SELECTED_QUANTITY));
	//
	// arrayofprolist.add(objItem);
	//
	// }
	// } catch (JSONException e) {
	// e.printStackTrace();
	// }
	//
	// Collections.sort(arrayofprolist, new Comparator<Products>() {
	//
	// @Override
	// public int compare(Products lhs, Products rhs) {
	// return (lhs.getProduct_id() - rhs.getProduct_id());
	// }
	// });
	// setAdapterToListview();
	//
	// }
	// }

	// }

	public void setAdapterToListview() {
		objAdapter = new ShopbyProductdetails(ShopbyPrice.this,
				R.layout.productsprice, arrayofprolist);
		listView.setAdapter(objAdapter);

	}

	public class ShopbyProductdetails extends ArrayAdapter<Products> {
		// ListView lv;
		Activity activity;
		private List<Products> items;
		private Products objBean;
		private int row;
		ArrayList<Products> Shop_Sort_Al = new ArrayList<Products>();
		boolean[] itemChecked;

		public ShopbyProductdetails(Activity act, int resource,
				List<Products> arrayofprolist) {
			super(act, resource, arrayofprolist);
			this.activity = act;
			this.row = resource;
			this.items = arrayofprolist;
			this.Shop_Sort_Al = new ArrayList<Products>();
			this.Shop_Sort_Al.addAll(arrayofprolist);
			itemChecked = new boolean[items.size()];
		}

		public int getCount() {
			return items.size();
		}

		public Products getItem(int position) {
			return items.get(position);
		}

		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;

			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(row, null);

				holder = new ViewHolder();
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			if ((items == null) || ((position + 1) > items.size()))
				return view;

			objBean = items.get(position);

			holder.pro_name = (TextView) view.findViewById(R.id.pro_name);
			holder.pro_quantity = (TextView) view.findViewById(R.id.pro_qty);
			holder.pro_pirce = (TextView) view.findViewById(R.id.pro_price);
			holder.pro_coupon = (TextView) view.findViewById(R.id.pro_coupon);
			holder.pro_total = (TextView) view.findViewById(R.id.pro_total);

			if (holder.pro_name != null && null != objBean.getName()
					&& objBean.getName().trim().length() > 0) {
				holder.pro_name.setText(Html.fromHtml(objBean.getName()));
			}

			holder.pro_pirce.setText(Html.fromHtml("$" + (objBean.getPrice())));

			if (holder.pro_quantity != null
					&& null != objBean.getSelected_quantity()) {
				holder.pro_quantity.setText(Html.fromHtml(objBean
						.getSelected_quantity()));
			}
			if (holder.pro_coupon != null && null != objBean.getCoupon_code()) {
				holder.pro_coupon.setText(Html.fromHtml(objBean
						.getCoupon_code()));
			}
			if (holder.pro_total != null && null != objBean.getFinal_price()) {
				holder.pro_total.setText(Html.fromHtml("$"
						+ objBean.getFinal_price()));
			}

			return view;
		}

		public class ViewHolder {
			public TextView pro_name, pro_quantity, pro_pirce, pro_coupon,
					pro_total, maps;
			public CheckBox check;
			public ImageView maps1;
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		if (pDialog != null)
			pDialog.dismiss();
		pDialog = null;
	}
}
