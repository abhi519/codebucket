package com.compare.mobishops;

public class Shops {

	private int id;
	
	private String name;
	private String latitude;
	private String longitude;
	private String total;
	private String noofproducts;
	
	public String getNoofproducts() {
		return noofproducts;
	}
	public void setNoofproducts(String noofproducts) {
		this.noofproducts = noofproducts;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	
	
	
}
