package com.example.mobishops;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.ClipData.Item;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class AllShopsActivity extends ListActivity implements OnItemClickListener {
	ListView listview;
	HashMap<String, String> map;
	//HashMap<Integer,String> ids;
	public static Activity rd; 
	int id=0;
	ListviewAdapter adapter;
	private ProgressDialog pDialog;
	//ArrayList<String> ids;
	private static final String READ_DETAILS_URL = "http://www.sesm.test1mcis.com/all_shops.php?";
	// manages all of our comments in a list.
	private ArrayList<HashMap<String, String>> detailsList;
	//private ArrayList<HashMap<Integer,String>> idslist;
	private static final String TAG_SHOPNAME = "name";
	private static final String TAG_DETAILS = "posts";
	private static final String TAG_ID = "id";
	private static final String TAG_ADDRESS = "address";
	private static final String TAG_LATITUDE = "latitude";
	private static final String TAG_LONGITUDE = "longitude";
	private static final String TAG_EMAIL = "email_address";
	private static final String TAG_CONTACT = "contact_number";
	private static final String TAG_CITY = "city";
	private static final String TAG_STATE = "state";
	private static final String TAG_ZIPCODE = "zipcode";
	private static final String TAG_COUNTRY = "country";

	// An array of all of our comments
	private JSONArray details = null, newarray = null;
	String userstatus;
	String userpassparameter,res,shopid,email;
	private ArrayList<Boolean> status = new ArrayList<Boolean>();
	
	ArrayList<Integer> positionArr = new ArrayList<Integer>();// to store list
																// of selected
	SharedPreferences prefs;
	Button add,ok;// positions
	ArrayList<String> idsstring;
	TextView tv1,tv2,tv3,tv4,show;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.globalshops_list);
		prefs=getSharedPreferences("com.example.mobishops", MODE_PRIVATE);
		email=SessionStore.getString(getApplicationContext(), SessionStore.EMAIL, "");
//		email=prefs.getString("oldmail", null);
		Log.d("allshops", ""+email);
	   add=(Button)findViewById(R.id.addshopbutton);
	
		listview = (ListView)findViewById(android.R.id.list);
		listview.setOnItemClickListener(this);
		listview.setFocusable(true);
		idsstring=new ArrayList<String>();
		add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			new Shops().execute();	
			}
		});
		new LoadShops().execute();

	}

	public void updateJSONdata() {

		detailsList = new ArrayList<HashMap<String, String>>();
		JSONParser jParser = new JSONParser();
		JSONObject json = jParser.getJSONFromUrl(READ_DETAILS_URL);

		try {
			details = json.getJSONArray(TAG_DETAILS);
			newarray = sortJsonArray(details);

			for (int i = 0; i < newarray.length(); i++) {

				JSONObject c = newarray.getJSONObject(i);

				String shopid = c.getString(TAG_ID);
				String shopname = c.getString(TAG_SHOPNAME);
				String address = c.getString(TAG_ADDRESS);
				String latitude = c.getString(TAG_LATITUDE);
				String longitude = c.getString(TAG_LONGITUDE);
				String email = c.getString(TAG_EMAIL);
				String contact = c.getString(TAG_CONTACT);
				String city = c.getString(TAG_CITY);
				String state = c.getString(TAG_STATE);
				String zipcode = c.getString(TAG_ZIPCODE);
				String country = c.getString(TAG_COUNTRY);

				map = new HashMap<String, String>();

				map.put(TAG_ID, shopid);
				map.put(TAG_SHOPNAME, shopname);
				map.put(TAG_ADDRESS, address);
				map.put(TAG_LATITUDE, latitude);
				map.put(TAG_LONGITUDE, longitude);
				map.put(TAG_EMAIL, email);
				map.put(TAG_CONTACT, contact);
				map.put(TAG_CITY, city);
				map.put(TAG_STATE, state);
				map.put(TAG_ZIPCODE, zipcode);
				map.put(TAG_COUNTRY, country);
				Collection<String> values = map.values();
				values.iterator();

				detailsList.add(map);

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public class LoadShops extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AllShopsActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... arg0) {

			updateJSONdata();
			return userpassparameter;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (pDialog != null) {
					pDialog.dismiss();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			adapter = new ListviewAdapter(getApplicationContext(), detailsList);
			listview.setAdapter(adapter);
			
			

		}
	}

	public class ListviewAdapter extends BaseAdapter {
		public Context mContext;

		public ListviewAdapter(Context context,
				ArrayList<HashMap<String, String>> list) {
			super();
			mContext = context;
			detailsList = list;
			for (int i = 0; i < list.size(); i++) {
				status.add(false);
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return detailsList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return detailsList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		class ViewHolder {
			TextView shopname;
			TextView shopstate;
			
		
			CheckBox checkBox;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub

			final ViewHolder holder;
			LayoutInflater inflater = getLayoutInflater();

			if (convertView == null) {

				convertView = inflater.inflate(R.layout.trails, null);

				holder = new ViewHolder();

				holder.shopname = (TextView) convertView
						.findViewById(R.id.textViewShopName);
				holder.shopstate = (TextView) convertView
						.findViewById(R.id.textViewShoplocated);
				holder.checkBox = (CheckBox) convertView
						.findViewById(R.id.checkBox1);
				
				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			map = detailsList.get(position);
			holder.shopname.setText(map.get(TAG_SHOPNAME));
			holder.shopstate.setText(map.get(TAG_STATE));
			holder.checkBox
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub					
							shopid=detailsList.get(position).get(TAG_ID).toString();
						//id=id+1;
							if (isChecked) {
								status.set(position, true);
								//idsstring.add(shopid);
							//	ids.put(id, shopid);
							//	idslist.add(ids);
								//Log.d("ids",""+idsstring);
								Log.d("status", "" + status);

								
							} else {
								status.set(position, false);
							//	ids.remove(id);
								//idslist.add(ids);
							//	idsstring.remove(shopid);

							//	Log.d("ids",""+ids);
								Log.d("status", "" + status);
							}
							/*if (isChecked) {
								status.set(position, true);
								
								if(!ids.contains(shopid)){
									ids.add(shopid);
									Log.d("shopids",""+ids);
								}
							//	else{
								//	ids.remove(shopid);		
								//	}
								Log.d("status", "" + status);

								
							} else {
								status.set(position, false);
								ids.remove(shopid);
								Log.d("shopids",""+ids);
								Log.d("status", "" + status);
							}
							//new Shops().execute();
*/
						}
					});
			holder.checkBox.setChecked(status.get(position));
			
			return convertView;

		}
	}

	public static JSONArray sortJsonArray(JSONArray array) {
		List<JSONObject> jsons = new ArrayList<JSONObject>();
		for (int i = 0; i < array.length(); i++) {
			try {
				jsons.add(array.getJSONObject(i));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Collections.sort(jsons, new Comparator<JSONObject>() {
			@Override
			public int compare(JSONObject lhs, JSONObject rhs) {
				Integer lid = null, rid = null;
				try {
					lid = Integer.parseInt(lhs.getString(TAG_ID));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// String rid = null;
				try {
					rid = Integer.parseInt(rhs.getString(TAG_ID));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// Here you could parse string id to integer and then compare.
				return lid.compareTo(rid);
			}

		});
		return new JSONArray(jsons);
	}

	
	public class Shops extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AllShopsActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... arg0) {

			ArrayList<NameValuePair> valueparams= new ArrayList<NameValuePair>();
			//Log.d("email",email);
			valueparams.add(new BasicNameValuePair("email_address",email));
			for(int i=0;i<status.size();i++){
				if(status.get(i)==true){
					idsstring.add(detailsList.get(i).get(TAG_ID).toString());
					Log.d("ksdbs",""+idsstring);
				}
			}
			for(int i=0;i<idsstring.size();i++){
			valueparams.add(new BasicNameValuePair("shop_id["+i+"]",idsstring.get(i)));
			//valueparams.add(new BasicNameValuePair("shop_id["+i+"]",idsstring.get(i)));
			//valueparams.add(new BasicNameValuePair("shop_id[2]","8"));
			Log.d("shop_id["+i+"]", idsstring.get(i));
			}
			
			valueparams.add(new BasicNameValuePair("mode","addshop"));
			

			String response=null;

			try{
				//http://www.sesm.test1mcis.com/userfav_shops.php

			response=CustomHttpClient.executeHttpPost("http://www.sesm.test1mcis.com/userfav_shops.php", valueparams);

			  res=response.toString();

			}catch(Exception e){

			e.printStackTrace();

			}


			return res;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.d("addshop",result);
			try {
				if (pDialog != null) {
					pDialog.dismiss();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(result.toString().replace(" ","").trim().equalsIgnoreCase("success")){
				Toast.makeText(getApplicationContext(), "Shops are added",Toast.LENGTH_SHORT).show();
			}
			else{
				Toast.makeText(getApplicationContext(), "Shops can not added",Toast.LENGTH_SHORT).show();
			}
			
			

		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int pos, long id) {
		// TODO Auto-generated method stub
		Activity a=this;
		final Dialog dialog = new Dialog(a);
		//dialog.setContentView(R.layout.customdialog);
		
		dialog.setContentView(R.layout.customdialog);
		dialog.setTitle("Shop Details");

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.shopname);
		TextView text1 = (TextView) dialog.findViewById(R.id.ADDRESS);
		TextView text2 = (TextView) dialog.findViewById(R.id.EMAIL);
		TextView text3 = (TextView) dialog.findViewById(R.id.MOBILE);
		
		text.setText(detailsList.get(pos).get(TAG_SHOPNAME)+".");
		text1.setText(detailsList.get(pos).get(TAG_ADDRESS)+" "+detailsList.get(pos).get(TAG_CITY)+".");
		text2.setText(detailsList.get(pos).get(TAG_STATE)+", "+detailsList.get(pos).get(TAG_COUNTRY)+", "+detailsList.get(pos).get(TAG_ZIPCODE)+".");
		text3.setText(detailsList.get(pos).get(TAG_EMAIL)+","+detailsList.get(pos).get(TAG_CONTACT)+".");
		ok = (Button) dialog.findViewById(R.id.OK);
		// if button is clicked, close the custom dialog
		ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}
	
}
