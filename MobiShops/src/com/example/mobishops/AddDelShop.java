package com.example.mobishops;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

public class AddDelShop extends FragmentActivity implements OnClickListener {
	ArrayList<HashMap<String, String>> shopslist;
	HashMap<String, String> map1;
	private ProgressDialog pDialog;
	private static final String TAG_SHOPNAME = "name";
	private static final String TAG_DETAILS = "posts";
	private static final String TAG_ID = "shop_id";
	private static final String READ_DETAILS_URL = "http://www.sesm.test1mcis.com/showuserfav_shops.php?email_address=";
	ListviewAdapter adapter;
	private JSONArray details = null;
	SharedPreferences p;
	String mail, userpassparameter, shopid, res;
	ListView listview;
	ArrayList<String> idsstring;
	private ArrayList<Boolean> status = new ArrayList<Boolean>();
	Button del, add;
	TextView t;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.adshop);
		p = getSharedPreferences("com.example.mobishops", MODE_PRIVATE);
//		mail = p.getString("oldmail", null);
		mail = SessionStore.getString(getApplicationContext(),
				SessionStore.EMAIL, "");
		Log.d("adddelshops", ""+mail);
		// listview=(ListView)findViewById(R.id.listView2);
		idsstring = new ArrayList<String>();
		// del=(Button)findViewById(R.id.deleteshopb);
		// add=(Button)findViewById(R.id.addshopb);
		// del.setOnClickListener(this);
		// add.setOnClickListener(this);
		// new LoadShops().execute();

	}

	public void updateJSONdata() {

		shopslist = new ArrayList<HashMap<String, String>>();
		JSONParser jParser = new JSONParser();
		JSONObject json = jParser.getJSONFromUrl(READ_DETAILS_URL + mail);

		try {
			details = json.getJSONArray(TAG_DETAILS);
			// newarray = sortJsonArray(details);

			for (int i = 0; i < details.length(); i++) {

				JSONObject c = details.getJSONObject(i);

				String shopid = c.getString(TAG_ID);
				String shopname = c.getString(TAG_SHOPNAME);
				/*
				 * String address = c.getString(TAG_ADDRESS); String latitude =
				 * c.getString(TAG_LATITUDE); String longitude =
				 * c.getString(TAG_LONGITUDE); String email =
				 * c.getString(TAG_EMAIL); String contact =
				 * c.getString(TAG_CONTACT); String city =
				 * c.getString(TAG_CITY); String state = c.getString(TAG_STATE);
				 * String zipcode = c.getString(TAG_ZIPCODE); String country =
				 * c.getString(TAG_COUNTRY);
				 */

				map1 = new HashMap<String, String>();

				map1.put(TAG_ID, shopid);
				map1.put(TAG_SHOPNAME, shopname);
				/*
				 * map.put(TAG_ADDRESS, address); map.put(TAG_LATITUDE,
				 * latitude); map.put(TAG_LONGITUDE, longitude);
				 * map.put(TAG_EMAIL, email); map.put(TAG_CONTACT, contact);
				 * map.put(TAG_CITY, city); map.put(TAG_STATE, state);
				 * map.put(TAG_ZIPCODE, zipcode); map.put(TAG_COUNTRY, country);
				 */
				Collection<String> values = map1.values();
				values.iterator();

				shopslist.add(map1);

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public class LoadShops extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AddDelShop.this);
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... arg0) {

			updateJSONdata();
			return userpassparameter;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (pDialog != null) {
					pDialog.dismiss();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (shopslist.size() == 0) {
				setContentView(R.layout.s);
				t = (TextView) findViewById(R.id.tt);
				t.setText("No Shops are selected from the List");
			} else {
				Log.d("bdvbk", "lm");
				setContentView(R.layout.adshop);
				listview = (ListView) findViewById(R.id.listView2);
				// idsstring=new ArrayList<String>();
				del = (Button) findViewById(R.id.deleteshopb);
				add = (Button) findViewById(R.id.addshopb);
				del.setOnClickListener(AddDelShop.this);
				add.setOnClickListener(AddDelShop.this);
				adapter = new ListviewAdapter(getApplicationContext(),
						shopslist);
				listview.setAdapter(adapter);
			}
			Log.d("shopslist", "" + shopslist);

		}
	}

	public class ListviewAdapter extends BaseAdapter {
		public Context mContext;

		public ListviewAdapter(Context context,
				ArrayList<HashMap<String, String>> list) {
			super();
			mContext = context;
			shopslist = list;
			for (int i = 0; i < list.size(); i++) {
				status.add(false);
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return shopslist.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return shopslist.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		class ViewHolder {
			TextView shopid;
			TextView shopname;
			// ImageButton deleteshop;
			/*
			 * TextView shopcountry; ImageView image;
			 */
			CheckBox checkBox;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub

			final ViewHolder holder;
			LayoutInflater inflater = getLayoutInflater();

			if (convertView == null) {

				convertView = inflater.inflate(R.layout.order_list, null);

				holder = new ViewHolder();

				holder.shopid = (TextView) convertView
						.findViewById(R.id.itemid);
				holder.shopname = (TextView) convertView
						.findViewById(R.id.itemname);
				// holder.deleteshop=(ImageButton)convertView.findViewById(R.id.deletebutton);

				holder.checkBox = (CheckBox) convertView
						.findViewById(R.id.check);

				/*
				 * holder.image = (ImageView) convertView
				 * .findViewById(R.id.imagearrow);
				 */

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			map1 = shopslist.get(position);
			holder.shopid.setText(map1.get(TAG_ID));
			holder.shopname.setText(map1.get(TAG_SHOPNAME));
			holder.checkBox
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							shopid = shopslist.get(position).get(TAG_ID)
									.toString();
							// id=id+1;
							if (isChecked) {
								status.set(position, true);
								idsstring.add(shopid);
								// ids.put(id, shopid);
								// idslist.add(ids);
								// Log.d("ids",""+ids);
								Log.d("status", "" + status);

							} else {
								status.set(position, false);
								// ids.remove(id);
								// idslist.add(ids);
								idsstring.remove(shopid);

								// Log.d("ids",""+ids);
								Log.d("status", "" + status);
							}
						}
					});
			holder.checkBox.setChecked(status.get(position));
			return convertView;

		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.deleteshopb:
			new Shops().execute();
			break;
		case R.id.addshopb:
			Intent passintent = new Intent(getApplicationContext(),
					AllShopsActivity.class);
			startActivity(passintent);
			break;
		}

	}

	public class Shops extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AddDelShop.this);
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... arg0) {

			ArrayList<NameValuePair> valueparams = new ArrayList<NameValuePair>();
			// Log.d("email",email);
			valueparams.add(new BasicNameValuePair("email_address", mail));

			for (int i = 0; i < idsstring.size(); i++) {
				valueparams.add(new BasicNameValuePair("shop_id[" + i + "]",
						idsstring.get(i)));
				// valueparams.add(new
				// BasicNameValuePair("shop_id["+i+"]",idsstring.get(i)));
				// valueparams.add(new BasicNameValuePair("shop_id[2]","8"));
				Log.d("shop_id[" + i + "]", idsstring.get(i));
			}

			valueparams.add(new BasicNameValuePair("mode", "delshop"));

			String response = null;

			try {
				// http://www.sesm.test1mcis.com/userfav_shops.php

				response = CustomHttpClient.executeHttpPost(
						"http://www.sesm.test1mcis.com/userfav_shops.php",
						valueparams);

				res = response.toString();

			} catch (Exception e) {

				e.printStackTrace();

			}

			return res;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.d("addshop", result);
			try {
				if (pDialog != null) {
					pDialog.dismiss();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (result.toString().replace(" ", "").trim()
					.equalsIgnoreCase("success")) {
				Toast.makeText(getApplicationContext(), "Shops are deleted",
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getApplicationContext(),
						"Shops can not deleted", Toast.LENGTH_SHORT).show();
			}

		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		new LoadShops().execute();
	}
}
