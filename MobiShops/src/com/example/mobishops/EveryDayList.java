package com.example.mobishops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.database.handlers.DatabaseHandler;
import com.database.handlers.ItemList;
import com.database.handlers.SpeialOrder;

public class EveryDayList extends FragmentActivity {
	DatabaseHandler dbHandler;
	ArrayList<HashMap<String, String>> Items;
	HashMap<String, String> map;
	DayItemsAdapter adapter;
	SharedPreferences pr;
	String email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.order);
		Items = new ArrayList<HashMap<String, String>>();
		dbHandler = new DatabaseHandler(getApplicationContext());
		email=SessionStore.getString(getApplicationContext(), SessionStore.EMAIL, "");
		Log.d("myfavlist", "" + email);
		List<ItemList> list = dbHandler.getAllItems(email);
		List<SpeialOrder> spl = dbHandler.getAllSItems(email);
		for (SpeialOrder cn : spl) {
			Log.d("edit", "oncreate()spl");
			String finallist = cn.get_id() + cn.getItemid() + cn.getCode()
					+ cn.getItemname();
			map = new HashMap<String, String>();
			map.put("itemId", cn.getItemid());
			map.put("itemcode", cn.getCode());
			map.put("itemname", cn.getItemname());
			Items.add(map);
			Log.d("details1", finallist);
		}
		for (ItemList cn : list) {
			Log.d("edit", "oncreate()2");
			String finallist = cn.get_id() + cn.getItemid() + cn.getCode()
					+ cn.getItemname();
			map = new HashMap<String, String>();
			map.put("itemId", cn.getItemid());
			map.put("itemcode", cn.getCode());
			map.put("itemname", cn.getItemname());
			Items.add(map);

			// Writing Contacts to log
			Log.d("details", finallist);

			dbHandler.close();
		}
		
		if (Items.size() == 0) {
//
//			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//					EveryDayList.this);
//			final SharedPreferences.Editor editor = pr.edit();
//			// set title
//			alertDialogBuilder.setTitle("");
//
//			// set dialog message
//			alertDialogBuilder
//					.setMessage(
//							"No Items are selected from Global List. Click on 'Select' button to select items from Global List.")
//					.setCancelable(false)
//					.setPositiveButton("Select",
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog,
//										int id) {
//									// if this button is clicked, close
//									editor.clear();
//									editor.putString("Key", "items");
//									editor.commit();
//									Intent list = new Intent(
//											getApplicationContext(),
//											MainActivity.class);
//									list.putExtra("url",
//											"http://www.sesm.test1mcis.com/all_products.php");
//									startActivity(list);
//								}
//							});
//
//			// create alert dialog
//			AlertDialog alertDialog = alertDialogBuilder.create();
//
//			// show it
//			alertDialog.show();
//
		} else {
			ListView lv = (ListView) findViewById(R.id.listView1);
			adapter = new DayItemsAdapter(Items);
			lv.setAdapter(adapter);
		}

	}

	public class DayItemsAdapter extends BaseAdapter {

		public DayItemsAdapter(ArrayList<HashMap<String, String>> list) {
			super();

			Items = list;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Items.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return Items.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder holderview;
			LayoutInflater inflater = getLayoutInflater();

			if (convertView == null) {

				convertView = inflater.inflate(R.layout.everydayitems, null);

				holderview = new ViewHolder();

				holderview.Ditemid = (TextView) convertView
						.findViewById(R.id.Ditemid);
				holderview.Ditemcode = (TextView) convertView
						.findViewById(R.id.Ditemcode);
				// holderview.Ditemname = (TextView) convertView
				// .findViewById(R.id.Ditemname);
				// holder.starbutton=(CheckBox)convertView.findViewById(R.id.check);

				convertView.setTag(holderview);
				// holder.starbutton.setTag(position);
				// //holder.starbutton.setChecked(sarray.get(position));
				/*
				 * holder.starbutton.setOnCheckedChangeListener(new
				 * OnCheckedChangeListener() {
				 * 
				 * @Override public void onCheckedChanged(CompoundButton
				 * buttonView, boolean isChecked) { // TODO Auto-generated
				 * method stub String
				 * s=detailsList.get(position).get(TAG_ITEMNAME).toString();
				 * Log.d("name",s); } });
				 */
			} else {
				holderview = (ViewHolder) convertView.getTag();
			}

			map = Items.get(position);
			holderview.Ditemid.setText(map.get("itemId"));
			holderview.Ditemcode.setText(map.get("itemname"));
			// holderview.Ditemname.setText(map.get("itemname"));

			return convertView;
		}

	}

	class ViewHolder {

		TextView Ditemname;
		TextView Ditemid;
		TextView Ditemcode;

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		dbHandler.close();
	}

}
