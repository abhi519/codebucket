package com.example.mobishops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.database.handlers.DatabaseHandler;
import com.database.handlers.ItemList;
import com.database.handlers.SpeialOrder;

public class AddDelItemList extends FragmentActivity {
	DatabaseHandler DbHandler;
	ArrayList<HashMap<String, String>> Items1;
	HashMap<String, String> map1;
	ADItemsAdapter adapter;
	TextView t;
	String email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.order);
		Items1 = new ArrayList<HashMap<String, String>>();
		DbHandler = new DatabaseHandler(getApplicationContext());
		email=SessionStore.getString(getApplicationContext(), SessionStore.EMAIL, "");
		Log.d("addordel", ""+email);

		List<ItemList> list = DbHandler.getAllItems(email);
		List<SpeialOrder> spl = DbHandler.getAllSItems(email);
		String idtemp;
		for (ItemList cnlist : list) {
			Log.d("edit", "oncreate()2");
			String finallist = cnlist.get_id() + cnlist.getItemid()
					+ cnlist.getCode() + cnlist.getItemname();
			map1 = new HashMap<String, String>();
			idtemp = ""+cnlist.get_id();
			map1.put("pkid", idtemp);
			map1.put("itemid", cnlist.getItemid());
			map1.put("itemcode", cnlist.getCode());
			map1.put("itemName", cnlist.getItemname());
			map1.put("email", cnlist.getEmailid());
			Items1.add(map1);

			// Writing Contacts to log

			DbHandler.close();
		}
		for (SpeialOrder cn : spl) {
			Log.d("edit", "oncreate()spl");
			String finallist = cn.get_id() + cn.getItemid() + cn.getCode()
					+ cn.getItemname();
			map1 = new HashMap<String, String>();
			idtemp = ""+cn.get_id();
			map1.put("pkid", idtemp);
			map1.put("itemid", cn.getItemid());
			map1.put("itemcode", cn.getCode());
			map1.put("itemName", cn.getItemname());
			Items1.add(map1);
			Log.d("details1", finallist);
		}
		if (Items1.size() == 0) {
			setContentView(R.layout.s);
			t = (TextView) findViewById(R.id.tt);
			t.setText("No Items are selected from the List");
		} else {
			setContentView(R.layout.order);
			ListView lv1 = (ListView) findViewById(R.id.listView1);
			adapter = new ADItemsAdapter(Items1);

			lv1.setAdapter(adapter);
		}

	}

	public class ADItemsAdapter extends BaseAdapter {

		public ADItemsAdapter(ArrayList<HashMap<String, String>> list) {
			super();

			Items1 = list;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Items1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return Items1.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder holderview;
			LayoutInflater inflater = getLayoutInflater();

			if (convertView == null) {

				convertView = inflater.inflate(R.layout.pricelist, null);

				holderview = new ViewHolder();

				holderview.aditemid = (TextView) convertView
						.findViewById(R.id.aditemid);

				holderview.aditemname = (TextView) convertView
						.findViewById(R.id.aditemname);
				holderview.deletebutton = (ImageButton) convertView
						.findViewById(R.id.deletebutton);
				convertView.setTag(holderview);

			} else {
				holderview = (ViewHolder) convertView.getTag();
			}

			// Log.d(";asdnckzb",""+Items1);
			map1 = Items1.get(position);

			holderview.aditemid.setText(map1.get("itemid"));
			holderview.aditemname.setText(map1.get("itemName"));
			holderview.deletebutton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					final Dialog dialog = new Dialog(AddDelItemList.this);
					// dialog.setContentView(R.layout.customdialog);

					dialog.setContentView(R.layout.custom);
					dialog.setTitle("Delete Item?");
					dialog.setCancelable(false);

					// set the custom dialog components - text, image and button
					TextView text = (TextView) dialog.findViewById(R.id.t1);
					TextView text1 = (TextView) dialog.findViewById(R.id.t2);
					TextView text2 = (TextView) dialog.findViewById(R.id.t3);
					Button yes = (Button) dialog.findViewById(R.id.yes);
					Button no = (Button) dialog.findViewById(R.id.no);

					text.setText("Are you sure to delete ");
					text1.setText(Items1.get(position).get("itemName"));
					text2.setText(" item?");

					// if button is clicked, close the custom dialog
					no.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dialog.cancel();
						}
					});
					yes.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dialog.cancel();
							int id = Integer.parseInt(Items1.get(position).get("pkid"));
							Boolean result = DbHandler.deleteItem(new ItemList(id,
									Items1.get(position).get("itemid"), Items1
											.get(position).get("itemName"),
									Items1.get(position).get("email")));
							if (result) {
								Log.d("resultdb", "" + result);
								Items1.remove(position);
								Toast.makeText(getApplicationContext(),
										"Item deleted", Toast.LENGTH_SHORT)
										.show();
								adapter.notifyDataSetChanged();
							}
						}
					});
					dialog.show();

				}
			});

			return convertView;
		}
	}

	class ViewHolder {

		TextView aditemid;
		TextView aditemname;

		ImageButton deletebutton;

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		DbHandler.close();
	}

}
