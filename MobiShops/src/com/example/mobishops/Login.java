package com.example.mobishops;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import android.text.Html;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.database.handlers.DatabaseHandler;
import com.database.handlers.SpinnerNavItem;
import com.database.handlers.TitleNavigationAdapter;

public class Login extends Activity implements OnClickListener,
		OnNavigationListener {

	Button b;
	TextView tv1, tv2, tv3, tv4, tv5, headingtext, t;
	EditText ed1, ed2;
	Intent i1, i2;
	String heading = "Welcome to:";
	String s = "101mobishops.com.";
	String heading1 = "<b>" + s + "</b>";
	String heading2 = "(Where you can shop easy and save more.)";
	String res;
	ActionBar actionbar;
	private TitleNavigationAdapter adapter;
	DatabaseHandler db;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);
		// ActionBar content
		actionbar = getActionBar();
		actionbar.setDisplayShowTitleEnabled(true);
		actionbar.setDisplayShowHomeEnabled(false);
		actionbar.setBackgroundDrawable(new ColorDrawable(Color.rgb(129, 159,
				247)));
		/*
		 * //actionbar.setTitle("Login");
		 * actionbar.setDisplayShowCustomEnabled(true);
		 * //actionbar.setNavigationMode(ActionBar.DISPLAY_SHOW_CUSTOM);
		 * 
		 * LayoutInflater inflater=(LayoutInflater)
		 * getApplicationContext().getSystemService
		 * (Context.LAYOUT_INFLATER_SERVICE);
		 * 
		 * View view=inflater.inflate(R.layout.action, null);
		 * actionbar.setCustomView(view); ImageButton
		 * but=(ImageButton)view.findViewById(R.id.imageButton); Spinner spinner
		 * = new Spinner(this);
		 */

		// this is the same adapter because I didn't want to spend time creating
		// a new one
		// ActionBarNavAdapter extends SpinnerAdapter of course

		/*
		 * final PopupMenu menu = new PopupMenu(this, but);
		 * menu.inflate(R.menu.activity_main_actions);
		 * menu.setOnMenuItemClickListener(this); but.setOnClickListener(new
		 * OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub menu.show();
		 * 
		 * } });
		 */
		/*
		 * navSpinner = new ArrayList<SpinnerNavItem>(); navSpinner.add(new
		 * SpinnerNavItem("Local", R.drawable.ic_launcher)); navSpinner .add(new
		 * SpinnerNavItem("My Places", R.drawable.ic_launcher));
		 * navSpinner.add(new SpinnerNavItem("Checkins",
		 * R.drawable.ic_launcher));
		 * 
		 * 
		 * // title drop down adapter adapter = new
		 * TitleNavigationAdapter(getApplicationContext(), navSpinner);
		 * spinner.setAdapter(adapter);
		 */

		// assigning the spinner navigation
		// actionbar.setListNavigationCallbacks(adapter, this);
		// actionbar.setSelectedNavigationItem(3);

		b = (Button) findViewById(R.id.button1);
		b.setOnClickListener(this);

		tv2 = (TextView) findViewById(R.id.textView2);
		t = (TextView) findViewById(R.id.t);
		tv4 = (TextView) findViewById(R.id.textView4);
		tv5 = (TextView) findViewById(R.id.textView5);

		ed1 = (EditText) findViewById(R.id.editText1);
		ed2 = (EditText) findViewById(R.id.editText2);

		tv4.setOnClickListener(this);
		tv5.setOnClickListener(this);

		TextPaint tp = new TextPaint();
		tp.linkColor = Color.BLUE; // not quite sure what the format should be
		UnderlineSpan us = new UnderlineSpan();
		us.updateDrawState(tp);
		SpannableString content = new SpannableString("Forgot Password?");
		content.setSpan(us, 0, content.length(), 0);

		tv4.setText(content);
		SpannableString content1 = new SpannableString("New Registrant?");
		content1.setSpan(new UnderlineSpan(), 0, content1.length(), 0);

		i1 = new Intent(getApplicationContext(), ForgetPassword.class);
		tv5.setText(content1);

		tv2.setText(Html.fromHtml(heading + " " + heading1));
		tv2.setTextSize(19);
		t.setText(heading2);
		t.setTextSize(14);

	}
@Override
protected void onResume() {
	super.onResume();
	ed1.setText("abhi519.ketha@gmail.com");
	ed2.setText("teamindia");
	
}
	public void onClick(View v) {
		Intent i;
		switch (v.getId()) {
		case R.id.textView4:
			i = new Intent(getApplicationContext(), ForgetPassword.class);
			startActivity(i);
			break;
		case R.id.textView5:
			i = new Intent(getApplicationContext(), Registration.class);
			startActivity(i);
			break;
		case R.id.button1:
			new Upload().execute();
			break;

		default:
			break;
		}

	}

	class Upload extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			// Create a new HttpClient and Post Header

			ArrayList<NameValuePair> valueparams = new ArrayList<NameValuePair>();

			valueparams.add(new BasicNameValuePair("email_address", ed1
					.getText().toString()));

			valueparams.add(new BasicNameValuePair("password", ed2.getText()
					.toString()));

			String response = null;

			try {

				response = CustomHttpClient.executeHttpPost(
						"http://www.sesm.test1mcis.com/login.php", valueparams);

				res = response.toString();

			} catch (Exception e) {

				e.printStackTrace();

			}

			return res;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			String ss = ed1.getText().toString();
			if (result.toString().replace(" ", "").trim().equalsIgnoreCase("1")) {
				Toast.makeText(getApplicationContext(), "Login Success",
						Toast.LENGTH_SHORT).show();

				SessionStore.saveString(getApplicationContext(),
						SessionStore.EMAIL, ss);

				Intent itemlist = new Intent(getApplicationContext(),
						ItemMenuList.class);
				itemlist.putExtra("oldmail", ss);
				startActivity(itemlist);
			} else {
				Toast.makeText(getApplicationContext(), "Login Failed",
						Toast.LENGTH_SHORT).show();

			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Take appropriate action for each action item click
		switch (item.getItemId()) {

		case R.id.logout:
			// location found
			Toast.makeText(getApplicationContext(), "logout",
					Toast.LENGTH_SHORT).show();
			return true;
		case R.id.changep:
			Toast.makeText(getApplicationContext(), "change pssword",
					Toast.LENGTH_SHORT).show();
			return true;
		case R.id.editp:
			Toast.makeText(getApplicationContext(), "edit profile",
					Toast.LENGTH_SHORT).show();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onNavigationItemSelected(int pos, long id) {
		// TODO Auto-generated method stub
		// actionbar.setSelectedNavigationItem(pos);
		Toast.makeText(getApplicationContext(), pos, 1000).show();

		return true;
	}

}
