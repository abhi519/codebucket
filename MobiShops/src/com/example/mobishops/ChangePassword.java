package com.example.mobishops;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ChangePassword extends Activity implements OnClickListener {
	
	TextView tv1,tv2,tv3;
	EditText ed1,ed2,ed3;
	Button b;
	String res;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_third);
		
		ed1=(EditText)findViewById(R.id.editText1);
		ed2=(EditText)findViewById(R.id.editText2);
		ed3=(EditText)findViewById(R.id.editText3);
		
		b=(Button)findViewById(R.id.button1);
		b.setOnClickListener(this);
		
	}

	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.button1)
		{
			new Upload().execute();
		}
	}
	class Upload extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			// Create a new HttpClient and Post Header

			ArrayList<NameValuePair> valueparams= new ArrayList<NameValuePair>();

			valueparams.add(new BasicNameValuePair("email_address",ed1.getText().toString()));

			valueparams.add(new BasicNameValuePair("password",ed2.getText().toString()));
			
			valueparams.add(new BasicNameValuePair("new_password",ed3.getText().toString()));

			String response=null;

			try{

			response=CustomHttpClient.executeHttpPost("http://www.sesm.test1mcis.com/change_password.php", valueparams);

			 res=response.toString();
			 Log.d("jasbcuisfb", res);

			}catch(Exception e){

			e.printStackTrace();

			}

			return res;
			
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.d("shjhu",result);
			if(result.toString().replace(" ","").trim().equalsIgnoreCase("6")){
				Toast.makeText(getApplicationContext(), "Your Password has Changed",Toast.LENGTH_SHORT).show();
			}
			else{				
			Toast.makeText(getApplicationContext(), "Please Enter Email-Id, Old Password correctly", Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

	

}
}
