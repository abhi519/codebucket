package com.example.mobishops;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.compare.mobishops.SampleShops;
import com.database.handlers.DatabaseHandler;
import com.database.handlers.ItemList;
import com.database.handlers.SpeialOrder;

public class CurrentOrderActivity extends ListActivity {
	DatabaseHandler dbHandler;
	HashMap<String, String> map;
	ListView currentlist;
	EveryDayList everyDayList;
	CurrentOrderAdapter adapter;
	String productids;
	private static final String READ_DETAILS_URL = "http://www.sesm.test1mcis.com/cart.php";
	private static final String SEND_COUPON_URL = "http://www.sesm.test1mcis.com/get_coupon.php";
	ArrayList<HashMap<String, String>> currentItems;
	private ArrayList<Boolean> checked = new ArrayList<Boolean>();
//	private ArrayList<Boolean> quantityVerf = new ArrayList<Boolean>();
//	private ArrayList<Boolean> unitVerf = new ArrayList<Boolean>();
//	private HashSet<Spinner> lvSpnSet = new HashSet<Spinner>();
	private ArrayList<CheckBox> lv_cb_arrlist = new ArrayList<CheckBox>();
	ArrayList<String> idproduct;
	ArrayList<String> coupons = new ArrayList<String>();
	Button send;
	String res;
	String quantity;
	SharedPreferences sh_Pref;
	String couponitemids;
	ProgressDialog pDialog;
	HttpResponse response;
	HttpEntity resEntity;
	String response_coupon;
	String response_str, email1;
	SharedPreferences prefs;
	private ArrayList<String> editvalues = new ArrayList<String>();
	ArrayList<Boolean> loopDecider = new ArrayList<Boolean>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.currentorder);
		email1 = SessionStore.getString(getApplicationContext(),
				SessionStore.EMAIL, "");
		Log.d("email", "" + email1);

		currentItems = new ArrayList<HashMap<String, String>>();
		dbHandler = new DatabaseHandler(getApplicationContext());
		idproduct = new ArrayList<String>();
		coupons = new ArrayList<String>();
		List<ItemList> list = dbHandler.getAllItems(email1);
		List<SpeialOrder> spl = dbHandler.getAllSItems(email1);
		for (SpeialOrder cn : spl) {
			Log.d("edit", "oncreate()spl");
			String finallist = cn.get_id() + cn.getItemid() + cn.getCode()
					+ cn.getItemname();
			map = new HashMap<String, String>();
			map.put("itemId", cn.getItemid());
			map.put("itemcode", cn.getCode());
			map.put("itemname", cn.getItemname());
			currentItems.add(map);
			Log.d("details1", finallist);
		}
		for (ItemList cn : list) {
			Log.d("edit", "oncreate()2");
			String finallist = cn.get_id() + cn.getItemid() + cn.getCode()
					+ cn.getItemname() + cn.getItemwgt();
			map = new HashMap<String, String>();
			map.put("itemId", cn.getItemid());
			map.put("itemcode", cn.getCode());
			map.put("itemname", cn.getItemname());
			map.put("itemwgt", cn.getItemwgt());
			currentItems.add(map);

			// Writing Contacts to log
			Log.d("details", finallist);

			dbHandler.close();
		}

		currentlist = (ListView) findViewById(android.R.id.list);
		send = (Button) findViewById(R.id.senditems);
		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				loopDecider.clear();
				if(lv_cb_arrlist.size() > 0){
					for(int i = 0;i < lv_cb_arrlist.size();i++)
					{
						ViewGroup row = (ViewGroup) lv_cb_arrlist.get(i).getParent();
						EditText quanET = (EditText) row.getChildAt(2);
						Spinner unitSpn = (Spinner) row.getChildAt(3);
						String quanStr = quanET.getText().toString();
						if(quanStr.length()>0){
							if(unitSpn.getSelectedItemPosition()==0)
								loopDecider.add(false);
							else
								loopDecider.add(true);
							}
						else
							loopDecider.add(false);
					}					
					if(loopDecider.contains(false))
						Toast.makeText(CurrentOrderActivity.this,"Select atleast one item and give valid quantity,units",
								Toast.LENGTH_SHORT).show();
					else
						new CurrentShops().execute();					
				}
					
				else
					Toast.makeText(CurrentOrderActivity.this,
							"Select atleast one item and give valid quantity,units",
							Toast.LENGTH_SHORT).show();
			}
		});
		adapter = new CurrentOrderAdapter(currentItems);
		currentlist.setAdapter(adapter);

	}

	class CurrentOrderAdapter extends BaseAdapter {
		// public ArrayList myItems = new ArrayList();

		public CurrentOrderAdapter(ArrayList<HashMap<String, String>> list) {
			super();

			currentItems = list;

			for (int i = 0; i < list.size(); i++) {
				checked.add(false);
				editvalues.add("");
				coupons.add("");
			}

		}

		public void refresh(ArrayList<HashMap<String, String>> currentItems1) {
			currentItems = currentItems1;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return currentItems.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return currentItems.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			final ViewHolder holder;

			LayoutInflater inflater = getLayoutInflater();

			if (convertView == null) {

				convertView = inflater
						.inflate(R.layout.currentorderitems, null);
				holder = new ViewHolder();
				holder.product = (TextView) convertView
						.findViewById(R.id.tproduct);
				holder.check = (CheckBox) convertView
						.findViewById(R.id.checkBox1);
				holder.units = (Spinner) convertView
						.findViewById(R.id.unitSpinner);

				holder.quantity = (EditText) convertView
						.findViewById(R.id.tquantity);

				holder.coupons = (EditText) convertView
						.findViewById(R.id.tcoupons);
				holder.autocoupon = (Button) convertView
						.findViewById(R.id.butcompare);

				holder.couponwatcher = new CouponWatcher();
				holder.coupons.addTextChangedListener(holder.couponwatcher);

				holder.watcher = new EditTextWatcher();
				holder.quantity.addTextChangedListener(holder.watcher);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();

			}
			holder.watcher.setTarget(position);
			holder.watcher.setSpinner(holder.units);
			holder.couponwatcher.setTarget(position);
			/*holder.coupons.setText(coupons.get(position));
			holder.quantity.setText(editvalues.get(position));*/
			holder.units.setEnabled(false);
			holder.quantity.setEnabled(false);
			map = currentItems.get(position);

			holder.product.setText(map.get("itemname"));
			holder.check.setTag(R.id.action_location_found, holder.quantity);
			holder.check.setTag(R.id.additem, holder.units);
			holder.check.setTag(R.id.ADDRESS, holder.coupons);
			holder.check
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							EditText quan = (EditText) buttonView
									.getTag(R.id.action_location_found);
							EditText coupon = (EditText) buttonView
									.getTag(R.id.ADDRESS);
							Spinner unitSpn = (Spinner) buttonView
									.getTag(R.id.additem);							
							// TODO Auto-generated method stub
							productids = currentItems.get(position)
									.get("itemId").toString();
							Log.d("product", "" + productids);
							if (isChecked) {
								checked.set(position, true);
								quan.setEnabled(true);
								lv_cb_arrlist.add((CheckBox) buttonView);
								if (idproduct.contains(productids)) {

								} else {

									idproduct.add(productids);
									Log.d("check", "" + checked);
									Log.d("id pro", "" + idproduct);
								}

							} else {
								lv_cb_arrlist.remove(buttonView);
								quan.setText("");
								coupon.setText("");
								quan.setEnabled(false);
								unitSpn.setSelection(0);
								unitSpn.setEnabled(false);
								checked.set(position, false);
								idproduct.remove(productids);
								editvalues.remove(position);
								Log.d("check", "" + checked);
								Log.d("id pro", "" + idproduct);
							}
						}
					});
	//		holder.check.setChecked(checked.get(position));
			holder.autocoupon.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					couponitemids = currentItems.get(position).get("itemId")
							.toString();
					Log.d("coupon", "" + couponitemids);
					new Couponcode(holder.coupons).execute();
				}
			});

			return convertView;

		}

	}

	class ViewHolder {
		Spinner units;
		CheckBox check;
		EditText quantity;
		EditText coupons;
		Button autocoupon;
		TextView product;
		EditTextWatcher watcher;
		CouponWatcher couponwatcher;

	}

	class CurrentShops extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(CurrentOrderActivity.this);
			pDialog.setMessage("Loading Data...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(READ_DETAILS_URL);

			try {
				// Add your data

				ArrayList<NameValuePair> valueparams = new ArrayList<NameValuePair>();

				valueparams
						.add(new BasicNameValuePair("email_address", email1));
				// Log.d("email", "" + email1);
				for (int i = 0; i < idproduct.size(); i++) {

					valueparams.add(new BasicNameValuePair("prod_quantities["
							+ idproduct.get(i) + "]", editvalues.get(i)));
					valueparams.add(new BasicNameValuePair("prod_coupons["
							+ idproduct.get(i) + "]", coupons.get(i)));
					httppost.setEntity(new UrlEncodedFormEntity(valueparams));

					Log.d("prod_quantities[" + idproduct.get(i) + "]",
							editvalues.get(i));
					Log.d("prod_coupons[" + idproduct.get(i) + "]",
							coupons.get(i));
				}

				// Execute HTTP Post Request
				response = httpclient.execute(httppost);
				resEntity = response.getEntity();
				response_str = EntityUtils.toString(resEntity);
				if (resEntity != null) {
				}
			}

			catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return response_str;

		}

		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			try {
				Log.d("response", result);
				JSONObject jsonObject = new JSONObject(response_str);
				if (response_str != null) {

					Intent obj_intent = new Intent(CurrentOrderActivity.this,
							SampleShops.class);
					Bundle b = new Bundle();
					b.putStringArrayList("produtsno", idproduct);
					b.putString("Array", jsonObject.toString());
					Log.d("Array", jsonObject.toString());
					obj_intent.putExtras(b);
					// obj_intent.putStringArrayListExtra("produtsno",
					// idproduct);
					Log.d("noofproducts", "" + idproduct);
					startActivity(obj_intent);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private class EditTextWatcher implements TextWatcher {

		private int target;
		@SuppressWarnings("unused")
		Spinner spinner;

		@SuppressWarnings("unused")
		public void setSpinner(Spinner spinner) {
			this.spinner = spinner;
		}

		public void setTarget(int target) {
			this.target = target;
		}

		@Override
		public void afterTextChanged(Editable s) {
			editvalues.set(target, s.toString());
			// coupons.set(target, s.toString());
			if (s.toString().length() > 0) {
				spinner.setEnabled(true);
			} else {
				spinner.setSelection(0);
				spinner.setEnabled(false);

			}

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}
	}

	private class CouponWatcher implements TextWatcher {

		private int target1;

		public void setTarget(int target) {
			this.target1 = target;
		}

		@Override
		public void afterTextChanged(Editable s) {

			coupons.set(target1, s.toString());
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

		}

	}

	class Couponcode extends AsyncTask<Void, Void, String> {

		EditText ed;

		public Couponcode(EditText coupons) {
			this.ed = coupons;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			pDialog = new ProgressDialog(CurrentOrderActivity.this);
			pDialog.setMessage("Loading Data...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(SEND_COUPON_URL);
			try {
				// Add your data

				ArrayList<NameValuePair> valueparams = new ArrayList<NameValuePair>();
				// Log.d("email",email);

				valueparams.add(new BasicNameValuePair("product_id",
						couponitemids));

				httppost.setEntity(new UrlEncodedFormEntity(valueparams));
				// Execute HTTP Post Request
				response = httpclient.execute(httppost);
				resEntity = response.getEntity();
				response_coupon = EntityUtils.toString(resEntity);
				if (resEntity != null) {
					Log.i("coupon item id is ", couponitemids);
					Log.i("Coupon code is", response_coupon);

				} else {

				}
			}

			catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return response_coupon;

		}

		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG)
					.show();
			ed.setText(result);
			pDialog.dismiss();

		}
	}

}
