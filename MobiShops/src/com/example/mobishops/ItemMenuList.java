package com.example.mobishops;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;



public class ItemMenuList extends FragmentActivity implements OnClickListener{
	Button order,orderlist,dayitemlist,aditemlist,gitemlist,fshops,adfshops,gshopslist;
	TextView changepwd,editprfl;
	Context context;
	Intent intent;
	ProgressDialog progressdialog;
	SharedPreferences pr;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context=getApplicationContext();
		setContentView(R.layout.menulist);
       // View rootView = inflater.inflate(R.layout.menulist, container, false);
        changepwd=(TextView)findViewById(R.id.changepwd);
        editprfl=(TextView)findViewById(R.id.editprfl);
        order=(Button)findViewById(R.id.butto1);
        orderlist=(Button)findViewById(R.id.button1);
        dayitemlist=(Button)findViewById(R.id.button8);
        aditemlist=(Button)findViewById(R.id.button3);
        gitemlist=(Button)findViewById(R.id.button4);
        fshops=(Button)findViewById(R.id.button9);
        adfshops=(Button)findViewById(R.id.button6);
        gshopslist=(Button)findViewById(R.id.button7);
        order.setOnClickListener(this);
        orderlist.setOnClickListener(this);
        changepwd.setOnClickListener(this);
        editprfl.setOnClickListener(this);
        dayitemlist.setOnClickListener(this);
        aditemlist.setOnClickListener(this);
        adfshops.setOnClickListener(this);
        gitemlist.setOnClickListener(this);
        fshops.setOnClickListener(this);
        gshopslist.setOnClickListener(this);
        pr=getSharedPreferences("com.example.mobishops",Context.MODE_PRIVATE);
        
       
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		SharedPreferences.Editor editor=pr.edit();
		switch(v.getId()){
		case R.id.changepwd:
			
			Intent changepwd=new Intent();
			changepwd.setClass(context, ChangePassword.class);
			startActivity(changepwd);			
			
			break;
		case R.id.editprfl:
			
			intent=new Intent();
			intent.setClass(context, EditProfile.class);
			startActivity(intent);	
			break;
		case R.id.butto1:
			intent=new Intent(getApplicationContext(), CurrentOrderActivity.class);
			startActivity(intent);
			break;
		case R.id.button1:
			//Toast.makeText(getActivity(), "button 1", Toast.LENGTH_SHORT).show();
			intent=new Intent(context, EveryDayList.class);
			startActivity(intent);
			break;
		case R.id.button8:
			intent=new Intent(context, AddDelItemList.class);
			startActivity(intent);
			break;
		case R.id.button3:	
			editor.clear();
			editor.putString("Key", "items");
			editor.commit();
			intent=new Intent(context, MainActivity.class);
			intent.putExtra("url","http://www.sesm.test1mcis.com/all_products.php");
			startActivity(intent);
			break;
		case R.id.button4:
			intent=new Intent(context, FavouriteShops.class);
			startActivity(intent);
			break;
		case R.id.button9:
			intent=new Intent(context, AddDelShop.class);
			startActivity(intent);
			break;
			
		case R.id.button6:
			intent=new Intent(context, AllShopsActivity.class);
			startActivity(intent);
			break;
		case R.id.button7:
			editor.clear();
			editor.putString("Key", "special");
			editor.commit();			
			intent=new Intent(context, MainActivity.class);
			intent.putExtra("url","http://www.sesm.test1mcis.com/all_products.php?category_type=s");
			startActivity(intent);
			break;
		}
		
		
	}

}
