package com.example.mobishops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.database.handlers.DatabaseHandler;
import com.database.handlers.ItemList;
import com.database.handlers.SpeialOrder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ProductList extends Activity implements OnClickListener {
	JSONObject jsonobject;
	JSONArray jsonarray;
	ListView listview;
	ItemListAdapter adapter;
	ProgressDialog mProgressDialog;
	ArrayList<HashMap<String, String>> arraylist;
	static String ITEM_ID = "item_id";
	static String ITEM_NAME = "item_name";
	static String ITEM_WEIGHT = "item_weight";
	public boolean isChecked;
	private ArrayList<Boolean> status = new ArrayList<Boolean>();
	Button additem;
	ArrayList<String> positionArr, itemids, itemnames, itemwghts; // to store
																	// list
	SharedPreferences pr; // of selected
	DatabaseHandler dbhandler; // positions
	// static String POPULATION = "population";
	String c_id, key;
	TextView view;
	String email;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from copy.xml
		setContentView(R.layout.copy);
		// Execute DownloadJSON AsyncTask
		positionArr = new ArrayList<String>();
		itemids = new ArrayList<String>();
		itemnames = new ArrayList<String>();
		itemwghts = new ArrayList<String>();
		additem = (Button) findViewById(R.id.additem);
		view = (TextView) findViewById(R.id.view);
		dbhandler = new DatabaseHandler(getApplicationContext());
		additem.setOnClickListener(this);
		Intent p = getIntent();
		email = SessionStore.getString(getApplicationContext(),
				SessionStore.EMAIL, "");
		Log.d("productlist", "" + email);
		pr = getSharedPreferences("com.example.mobishops", MODE_PRIVATE);

		key = pr.getString("Key", null);
		c_id = p.getStringExtra("itemid");
		new DownloadJSON().execute();
		if (key.equals("items")) {

			view.setText("Select Items for Favorite List.");
		} else if (key.equals("special")) {
			// Toast.makeText(getApplicationContext(),
			// key,Toast.LENGTH_SHORT).show();
			view.setText("Select Items for Special Order.");
		}

	}

	// DownloadJSON AsyncTask
	private class DownloadJSON extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(ProductList.this);
			// Set progressdialog title
			// mProgressDialog.setTitle("Items List");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading Items...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// Create an array
			arraylist = new ArrayList<HashMap<String, String>>();
			// Retrieve JSON Objects from the given URL address
			JSONParser jparser = new JSONParser();
			jsonobject = jparser
					.getJSONFromUrl("http://www.sesm.test1mcis.com/productsbycategory.php?category_id="
							+ c_id);

			try {
				// Locate the array name in JSON
				jsonarray = jsonobject.getJSONArray("posts");

				for (int i = 0; i < jsonarray.length(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					jsonobject = jsonarray.getJSONObject(i);
					// Retrive JSON Objects
					map.put("item_id", jsonobject.getString("id"));
					map.put("item_name", jsonobject.getString("name"));
					map.put("item_weight", jsonobject.getString("weight_mode"));
					// map.put("category_image",
					// jsonobject.getString("category_image"));
					// Set the JSON Objects into the array
					arraylist.add(map);
				}
			} catch (JSONException e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {
			// Locate the listview in listview_main.xml
			listview = (ListView) findViewById(R.id.listview);
			// Pass the results into ListViewAdapter.java
			adapter = new ItemListAdapter(ProductList.this, arraylist);
			// Set the adapter to the ListView
			listview.setAdapter(adapter);
			// Close the progressdialog
			mProgressDialog.dismiss();
		}
	}

	public class ItemListAdapter extends BaseAdapter {

		// Declare Variables
		Context context;
		LayoutInflater inflater;
		// ArrayList<HashMap<String, String>> data;
		ImageLoader imageLoader;
		HashMap<String, String> itemp = new HashMap<String, String>();

		public ItemListAdapter(Context context,
				ArrayList<HashMap<String, String>> list) {
			this.context = context;
			arraylist = list;
			for (int i = 0; i < list.size(); i++) {
				status.add(false);

			}

		}

		@Override
		public int getCount() {
			return arraylist.size();
		}

		@Override
		public Object getItem(int position) {
			return arraylist.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// Declare Variables
			ViewHolder holder;

			// if(convertView==null){
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.item_list, parent, false);
			// Get the position
			itemp = arraylist.get(position);

			// Locate the TextViews in listview_item.xml
			holder.item_ids = (TextView) convertView.findViewById(R.id.itemid);
			holder.item_names = (TextView) convertView
					.findViewById(R.id.itemname);
			holder.selectCheck = (CheckBox) convertView
					.findViewById(R.id.check);

			convertView.setTag(convertView);
			// }
			// else{
			// // holder = (ViewHolder) convertView.getTag();
			// }

			// Capture position and set results to the TextViews

			holder.selectCheck
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							String prid = arraylist.get(position).get(ITEM_ID)
									.toString();
							String prname = arraylist.get(position)
									.get(ITEM_NAME).toString();
							String prweight = arraylist.get(position)
									.get(ITEM_WEIGHT).toString();
							Log.d("prwoqhfn", prweight);

							if (isChecked) {
								status.set(position, true);
								// positionArr.add(arraylist.get(position).get(ITEM_ID));
								if (!(itemids.contains(prid)
										&& itemnames.contains(prname) && itemwghts
										.contains(prweight))) {
									itemids.add(prid);
									itemnames.add(prname);
									itemwghts.add(prweight);
									Log.d("", "" + itemids);
									Log.d("", "" + itemnames);
									Log.d("", "" + itemwghts);
								}
								/*
								 * else{ itemids.remove(prid);
								 * itemnames.remove(prname); }
								 */
								// Toast.makeText(context, "Item"+positionArr,
								// 2000).show();
							} else {
								status.set(position, false);
								// positionArr.remove(arraylist.get(position).get(ITEM_ID));
								itemids.remove(prid);
								itemnames.remove(prname);
								itemwghts.remove(prweight);
								Log.d("", "" + itemids);
								Log.d("", "" + itemnames);
								Log.d("", "" + itemwghts);
							}

						}
					});
			// Capture ListView item click
			holder.selectCheck.setChecked(status.get(position));
			holder.item_ids.setText(itemp.get(ProductList.ITEM_ID));
			holder.item_names.setText(itemp.get(ProductList.ITEM_NAME));

			return convertView;
		}

	}

	class ViewHolder {
		TextView item_ids;
		TextView item_names;
		CheckBox selectCheck;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (key.equals("items")) {
			Log.d("ids,names", itemids + " " + itemnames);
			if (itemids.size() == itemnames.size()) {
				for (int i = 0; i < itemids.size(); i++) {

					checkDatabase(itemids.get(i), itemnames.get(i),
							itemwghts.get(i));
					Toast.makeText(getApplicationContext(),
							"Items are added to Favotite List",
							Toast.LENGTH_SHORT).show();

				}
			}
		} else if (key.equals("special")) {
			Log.d("ids,names", itemids + " " + itemnames);
			if (itemids.size() == itemnames.size()) {
				for (int i = 0; i < itemids.size(); i++) {

					checkDatabaseForSpecial(itemids.get(i), itemnames.get(i));
					Toast.makeText(getApplicationContext(),
							"Items are added to Favotite List",
							Toast.LENGTH_SHORT).show();

				}
			}
		}
	}

	void checkDatabase(String id, String name, String weight) {
		if (dbhandler.CheckIsDataAlreadyInDBorNot(email, id, name)) {
			Log.d("data ", "already inserted");
			List<ItemList> list = dbhandler.getAllItems(email);
			for (ItemList cn : list) {
				Log.d("edit", "oncreate()2");
				String finallist = cn.get_id() + cn.getItemid() + cn.getCode()
						+ cn.getItemname() + cn.getItemwgt();
				// Writing Contacts to log
				// Log.d("details",finallist);

				dbhandler.close();

			}
		} else {
			dbhandler.addItemList(new ItemList(email, id, name, weight));
			List<ItemList> list = dbhandler.getAllItems(email);
			for (ItemList cn : list) {
				Log.d("edit1", "oncreate()2");
				String finallist = cn.get_id() + cn.getItemid() + cn.getCode()
						+ cn.getItemname() + cn.getItemwgt();
				// Writing Contacts to log
				// Log.d("details",finallist);

				dbhandler.close();
			}
		}
	}

	void checkDatabaseForSpecial(String id, String name) {
		if (dbhandler.CheckIsDataAlreadyInDBorNot(email, id, name)) {
			Log.d("data ", "already inserted");
			List<SpeialOrder> list = dbhandler.getAllSItems(email);
			for (SpeialOrder cn : list) {
				Log.d("edit", "oncreate()2");
				String finallist = cn.get_id() + cn.getItemid() + cn.getCode()
						+ cn.getItemname();
				// Writing Contacts to log
				// Log.d("details",finallist);

				dbhandler.close();

			}
		} else {
			dbhandler.addSItemList(new SpeialOrder(email, id, name));
			List<SpeialOrder> list = dbhandler.getAllSItems(email);
			for (SpeialOrder cn : list) {
				Log.d("edit1", "oncreate()2");
				String finallist = cn.get_id() + cn.getItemid() + cn.getCode()
						+ cn.getItemname();
				// Writing Contacts to log
				Log.d("details", finallist);

				dbhandler.close();
			}
		}
	}
}
