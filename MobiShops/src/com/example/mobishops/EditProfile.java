package com.example.mobishops;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.database.handlers.Contact;
import com.database.handlers.DatabaseHandler;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditProfile extends Activity {

	EditText emailid1, password1,  firstname1, middlename1, lastname1,
	homecontact1, cellcontact1, address, zip1;	
	Button edit;
	HttpResponse response;
	HttpEntity resEntity;
	String response_str;
	DatabaseHandler dba;
	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	String oldmail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editprofile);
	Log.d("edit","oncreate()");
		emailid1 = (EditText) findViewById(R.id.EditTextUsername1);
		password1 = (EditText) findViewById(R.id.EditTextPassword1);
		//reenter1 = (EditText) findViewById(R.id.EditTextReenter1);
		firstname1 = (EditText) findViewById(R.id.EditTextfirstname1);
		middlename1 = (EditText) findViewById(R.id.EditTextmiddlename1);
		lastname1 = (EditText) findViewById(R.id.EditTextlastname1);
		homecontact1 = (EditText) findViewById(R.id.EditTextHomeContact1);
		cellcontact1 = (EditText) findViewById(R.id.EditTextCellContact1);
		address=(EditText) findViewById(R.id.addressedit);
		zip1 = (EditText) findViewById(R.id.EditTextZip1);
		edit = (Button) findViewById(R.id.buttonReporteredit);
		dba=new DatabaseHandler(getApplicationContext());
		preferences=getSharedPreferences("com.example.mobishops", MODE_PRIVATE);
		oldmail=preferences.getString("oldmail",null);
		Log.d("edit","oncreate()1");
		
		
		List<Contact> contacts = dba.getAllContacts(oldmail);       
		 
        for (Contact cn : contacts) {
        	Log.d("edit","oncreate()2");
            String id = cn.getID()+cn.get_emailid()+cn.getPassword()+cn.get_fname()+cn.get_mname()+cn.get_lname()+cn.get_phone_number()+cn.getHome_phone_number()+cn.getAddress()+cn.getZip();
                // Writing Contacts to log
            Log.d("details",id);
            emailid1.setText(cn.get_emailid());
            password1.setText(cn.getPassword());
           // reenter1.setText(cn.getPassword());
            firstname1.setText(cn.get_fname());
            middlename1.setText(cn.get_mname());
            lastname1.setText(cn.get_lname());
            homecontact1.setText(cn.getHome_phone_number());
            cellcontact1.setText(cn.get_phone_number());
            address.setText(cn.getAddress());
            zip1.setText(cn.getZip());
            dba.close();
        }
		
		edit.setOnClickListener(new OnClickListener() {
			
			@Override
		public void onClick(View v) {
				// TODO Auto-generated method stub
			
				new EditRegister().execute();
			}
		
		});
		
	}
	
	class EditRegister extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub

			// Create a new HttpClient and Post Header
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://www.sesm.test1mcis.com/edit_profile.php");
			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						7);
				//nameValuePairs.add(new BasicNameValuePair("user_id", userid));
				nameValuePairs.add(new BasicNameValuePair("old_email_address",
						oldmail));
				nameValuePairs.add(new BasicNameValuePair("first_name",
						firstname1.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("middle_name",
						middlename1.getText().toString()));
				nameValuePairs
						.add(new BasicNameValuePair("last_name", lastname1.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("email_address",
						emailid1.getText().toString()));
				nameValuePairs
						.add(new BasicNameValuePair("password", password1.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("cell_number",
						cellcontact1.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("home_number",
						homecontact1.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("address", address.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("zipcode",  zip1.getText().toString()));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				response = httpclient.execute(httppost);
				resEntity = response.getEntity();
				response_str = EntityUtils.toString(resEntity);
				if (resEntity != null) {
					Log.i("RESPONSE", response_str);
				}

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return response_str;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.d("response",result);
			if(result.toString().replace(" ", "").trim().equalsIgnoreCase("8")){
				Toast.makeText(getApplicationContext(), "Edit Successfully", Toast.LENGTH_SHORT).show();
				
				dba.updateContact(new Contact(oldmail, emailid1.getText().toString(), password1.getText().toString(),
						firstname1.getText().toString(),middlename1.getText().toString(),lastname1.getText().toString(), cellcontact1.getText().toString(), 
						homecontact1.getText().toString(), address.getText().toString(), zip1.getText().toString()));
				editor=preferences.edit();
				editor.putString("oldmail",emailid1.getText().toString());
				editor.commit();
				Log.d("old new",oldmail+" "+emailid1.getText().toString());
				
			}else{
				Toast.makeText(getApplicationContext(), "Edit Failed", Toast.LENGTH_SHORT).show();
			}
			

		}
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		dba.close();
	}
 
}
