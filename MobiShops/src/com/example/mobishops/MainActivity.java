package com.example.mobishops;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	// Declare Variables
	JSONObject jsonobject;
	JSONArray jsonarray;
	ListView listview;
	ListViewAdapter adapter;
	ProgressDialog mProgressDialog;
	ArrayList<HashMap<String, String>> arraylist;
	static String CATEGORY_ID = "category_id";
	static String CATEGORY_NAME = "category_name";
	//static String POPULATION = "population";
	static String CATEGORY_Img = "category_image";
	Typeface tf;
	String Url;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from listview_main.xml
		setContentView(R.layout.newxml);
		// Execute DownloadJSON AsyncTask
		
        tf = Typeface.createFromAsset(getAssets(), "Jester.ttf");
        Intent intent=getIntent();
        Url=intent.getStringExtra("url");
        new DownloadJSON().execute();
        
	}

	// DownloadJSON AsyncTask
	private class DownloadJSON extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(MainActivity.this);
			// Set progressdialog title
			mProgressDialog.setTitle("MobiShops Category List");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// Create an array
			arraylist = new ArrayList<HashMap<String, String>>();
			// Retrieve JSON Objects from the given URL address
			JSONParser jParser = new JSONParser();
			jsonobject = jParser.getJSONFromUrl(Url);
					//"http://www.sesm.test1mcis.com/all_products.php");

			try {
				// Locate the array name in JSON
				jsonarray = jsonobject.getJSONArray("posts");

				for (int i = 0; i < jsonarray.length(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					jsonobject = jsonarray.getJSONObject(i);
					// Retrive JSON Objects
					map.put("category_id", jsonobject.getString("category_id"));
					map.put("category_name", jsonobject.getString("category_name"));
					map.put("category_image", jsonobject.getString("category_image"));
					// Set the JSON Objects into the array
					arraylist.add(map);
				}
			} catch (JSONException e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {
			// Locate the listview in listview_main.xml
			listview = (ListView) findViewById(R.id.listView2);
			// Pass the results into ListViewAdapter.java
			adapter = new ListViewAdapter(MainActivity.this, arraylist);
			// Set the adapter to the ListView
			listview.setAdapter(adapter);
			// Close the progressdialog
			mProgressDialog.dismiss();
			
			listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int posn, long arg3) {
					// TODO Auto-generated method stub
					//Toast.makeText(getApplicationContext(), "Category "+(arraylist.get(posn).get("category_id")), 2000).show();
					String id=arraylist.get(posn).get("category_id");
					Intent i=new Intent(getApplicationContext(), ProductList.class);
					i.putExtra("itemid", id);
					startActivity(i);
				}
			});
		}
	}
	
	public class ListViewAdapter extends BaseAdapter {

		// Declare Variables
		Context context;
		LayoutInflater inflater;
		ArrayList<HashMap<String, String>> data;
		ImageLoader imageLoader;
		HashMap<String, String> resultp = new HashMap<String, String>();

		public ListViewAdapter(Context context,
				ArrayList<HashMap<String, String>> arraylist) {
			this.context = context;
			data = arraylist;
			imageLoader = new ImageLoader(context);
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {
			// Declare Variables
			TextView cat_ids;
			TextView cat_names;
			
			ImageView cat_images;

			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View itemView = inflater.inflate(R.layout.listview_item, parent, false);
			// Get the position
			resultp = data.get(position);

			// Locate the TextViews in listview_item.xml
		 cat_ids = (TextView) itemView.findViewById(R.id.cat_id);
			cat_names = (TextView) itemView.findViewById(R.id.cat_name);
			
            cat_names.setTypeface(tf);
			// Locate the ImageView in listview_item.xml
			cat_images = (ImageView) itemView.findViewById(R.id.cat_image);

			// Capture position and set results to the TextViews
			//cat_ids.setText(resultp.get(MainActivity.CATEGORY_ID));
			cat_names.setText(resultp.get(MainActivity.CATEGORY_NAME));
			
			imageLoader.DisplayImage(resultp.get(MainActivity.CATEGORY_Img), cat_images);
			// Capture ListView item click
			
			
			return itemView;
		}
	}
}