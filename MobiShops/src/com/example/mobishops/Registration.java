package com.example.mobishops;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import com.database.handlers.Contact;
import com.database.handlers.DatabaseHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registration extends Activity {
	EditText emailid, password, reenter, firstname, middlename, lastname,
			homecontact, cellcontact, address, address1, address2, zip;
	Button submit;
	ProgressDialog progressdialog;
	String res;
	DatabaseHandler db;
	String faddress;
	String email, pwd, renter, fname, mname, lname, hcontact, ccontact, addr,
			addr1, addr2, zipcode;
	Builder dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		emailid = (EditText) findViewById(R.id.EditTextUsername);
		password = (EditText) findViewById(R.id.EditTextPassword);
		reenter = (EditText) findViewById(R.id.EditTextReenter);
		firstname = (EditText) findViewById(R.id.EditTextfirstname);
		middlename = (EditText) findViewById(R.id.EditTextmiddlename);
		lastname = (EditText) findViewById(R.id.EditTextlastname);
		homecontact = (EditText) findViewById(R.id.EditTextHomeContact);
		cellcontact = (EditText) findViewById(R.id.EditTextCellContact);
		address = (EditText) findViewById(R.id.EditTextAddress);
		address1 = (EditText) findViewById(R.id.EditTextAddress1);
		address2 = (EditText) findViewById(R.id.EditTextAddress2);
		zip = (EditText) findViewById(R.id.EditTextZip);
		submit = (Button) findViewById(R.id.buttonReporterSubmit);
		db = new DatabaseHandler(this);

		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				// TODO Auto-generated method stub
				int atpos = emailid.getText().toString().indexOf("@");
				int dotpos = emailid.getText().toString().lastIndexOf(".");
				if (emailid.getText().toString().trim().length() == 0
						|| password.getText().toString().trim().length() == 0
						|| reenter.getText().toString().trim().length() == 0
						|| firstname.getText().toString().trim().length() == 0
						|| middlename.getText().toString().trim().length() == 0
						|| lastname.getText().toString().trim().length() == 0
						|| homecontact.getText().toString().trim().length() == 0
						|| cellcontact.getText().toString().trim().length() == 0
						|| address.getText().toString().trim().length() == 0
						|| address1.getText().toString().trim().length() == 0
						|| address2.getText().toString().trim().length() == 0
						|| zip.getText().toString().trim().length() == 0) {
					Toast.makeText(getApplicationContext(),
							"Please Enter All Fields", Toast.LENGTH_SHORT)
							.show();

				} else if (atpos < 1 || dotpos <= atpos + 2
						|| dotpos + 2 > emailid.toString().length()) { // check
					// the
					// EmailId
					// Format
					Toast.makeText(getApplicationContext(),
							"please enter valid email id", Toast.LENGTH_SHORT)
							.show();
				} else if (password.getText().toString().trim().length() < 6
						|| password.getText().toString().trim().length() > 12) {
					// check
					// the EmailId Format
					Toast.makeText(
							getApplicationContext(),

							"please enter password morethan 6 and lessthan 12 characters ",
							Toast.LENGTH_SHORT).show();

				}

				else if (emailid.getText().toString().trim().length() > 0
						|| password.getText().toString().trim().length() > 0
						|| reenter.getText().toString().trim().length() > 0
						|| firstname.getText().toString().trim().length() > 0
						|| middlename.getText().toString().trim().length() > 0
						|| lastname.getText().toString().trim().length() > 0
						|| homecontact.getText().toString().trim().length() > 0
						|| cellcontact.getText().toString().trim().length() > 0
						|| address.getText().toString().trim().length() > 0
						|| address1.getText().toString().trim().length() > 0
						|| address2.getText().toString().trim().length() > 0
						|| zip.getText().toString().trim().length() > 0) {
					email = emailid.getText().toString();
					pwd = password.getText().toString();
					renter = reenter.getText().toString();
					fname = firstname.getText().toString();
					mname = middlename.getText().toString();
					lname = lastname.getText().toString();
					hcontact = homecontact.getText().toString();
					ccontact = cellcontact.getText().toString();
					addr = address.getText().toString();
					addr1 = address1.getText().toString();
					addr2 = address2.getText().toString();
					zipcode = zip.getText().toString();
					new Register().execute();
				}
			}

		});
	}

	class Register extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressdialog = new ProgressDialog(Registration.this);
			progressdialog.setMessage("Verifying...");
			progressdialog.setIndeterminate(false);
			progressdialog.setCancelable(false);
			progressdialog.setProgressStyle(BIND_ADJUST_WITH_ACTIVITY);
			progressdialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub

			// Create a new HttpClient and Post Header

			// Add your data
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("first_name", firstname
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("middle_name", middlename
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("last_name", lastname
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("email_address", emailid
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("password", password
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("cell_number",
					cellcontact.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("home_number",
					homecontact.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("address", address
					.getText().toString()
					+ address1.getText().toString()
					+ address2.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("zipcode", zip.getText()
					.toString()));
			String response = null;

			try {

				response = CustomHttpClient.executeHttpPost(
						"http://www.sesm.test1mcis.com/register.php",
						nameValuePairs);

				res = response.toString();

			} catch (Exception e) {

				e.printStackTrace();

			}

			return res;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressdialog.cancel();
			Log.d("response", result);
			faddress = addr + addr1 + addr2;
			if (result.toString().replace(" ", "").trim().equalsIgnoreCase("2")) {
				Toast.makeText(getApplicationContext(),
						"Registered Successfully", Toast.LENGTH_SHORT).show();

				db.addContact(new Contact(email, email, pwd, fname, mname,
						lname, ccontact, hcontact, faddress, zipcode));
				SharedPreferences preferences = getSharedPreferences(
						"com.example.mobishops", MODE_PRIVATE);
				SharedPreferences.Editor edit = preferences.edit();
				edit.putString("oldmail", email);

				edit.commit();
				Log.d("oldmail", email);
				db.close();
				Intent i = new Intent(getApplicationContext(), Login.class);
				startActivity(i);
			} else {
				Toast.makeText(getApplicationContext(), "Not Registerd",
						Toast.LENGTH_SHORT).show();
			}

		}

	}
}
