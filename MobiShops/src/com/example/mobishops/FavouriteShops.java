package com.example.mobishops;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.mobishops.AllShopsActivity.ListviewAdapter;
import com.example.mobishops.AllShopsActivity.LoadShops;
import com.example.mobishops.AllShopsActivity.Shops;
import com.example.mobishops.AllShopsActivity.ListviewAdapter.ViewHolder;
import com.example.mobishops.EveryDayList.DayItemsAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class FavouriteShops extends FragmentActivity {
	ListView listview;
	HashMap<String, String> map;

	ListviewAdapter adapter;
	private ProgressDialog pDialog;
	private static final String READ_DETAILS_URL = "http://www.sesm.test1mcis.com/showuserfav_shops.php?email_address=";
	// manages all of our comments in a list.
	private ArrayList<HashMap<String, String>> detailsList;
	private static final String TAG_SHOPNAME = "name";
	private static final String TAG_DETAILS = "posts";
	private static final String TAG_ID = "shop_id";
	// private static final String TAG_ADDRESS = "address";
	// private static final String TAG_LATITUDE = "latitude";
	// private static final String TAG_LONGITUDE = "longitude";
	// private static final String TAG_EMAIL = "email_address";
	// private static final String TAG_CONTACT = "contact_number";
	// private static final String TAG_CITY = "city";
	// private static final String TAG_STATE = "state";
	// private static final String TAG_ZIPCODE = "zipcode";
	// private static final String TAG_COUNTRY = "country";

	// An array of all of our comments
	private JSONArray details = null, newarray = null;
	String userstatus;
	String userpassparameter, res, shopid, email;
	public boolean isChecked;
	private ArrayList<Boolean> status = new ArrayList<Boolean>();
	ArrayList<View> arr = new ArrayList<View>();// to store list of selected
												// views
	ArrayList<Integer> positionArr = new ArrayList<Integer>();// to store list
	SharedPreferences pr; // of selected
	SharedPreferences prefs; // positions

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.order);
		prefs = getSharedPreferences("com.example.mobishops", MODE_PRIVATE);
		email = SessionStore.getString(getApplicationContext(),
				SessionStore.EMAIL, "");
		Log.d("favshops", "" + email);
//		email = prefs.getString("oldmail", null);

		listview = (ListView) findViewById(R.id.listView1);
		// listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		pr = getSharedPreferences("com.example.mobishops", Context.MODE_PRIVATE);
		new LoadShops().execute();

	}

	public void updateJSONdata() {

		detailsList = new ArrayList<HashMap<String, String>>();
		JSONParser jParser = new JSONParser();
		JSONObject json = jParser.getJSONFromUrl(READ_DETAILS_URL + email);

		try {
			details = json.getJSONArray(TAG_DETAILS);
			// newarray = sortJsonArray(details);

			for (int i = 0; i < details.length(); i++) {

				JSONObject c = details.getJSONObject(i);

				String shopid = c.getString(TAG_ID);
				String shopname = c.getString(TAG_SHOPNAME);
				/*
				 * String address = c.getString(TAG_ADDRESS); String latitude =
				 * c.getString(TAG_LATITUDE); String longitude
				 * =c.getString(TAG_LONGITUDE); String email
				 * =c.getString(TAG_EMAIL); String contact
				 * =c.getString(TAG_CONTACT); String city =
				 * c.getString(TAG_CITY); String state = c.getString(TAG_STATE);
				 * String zipcode = c.getString(TAG_ZIPCODE); String country
				 * =c.getString(TAG_COUNTRY);
				 */

				map = new HashMap<String, String>();

				map.put(TAG_ID, shopid);
				map.put(TAG_SHOPNAME, shopname);
				/*
				 * map.put(TAG_ADDRESS, address); map.put(TAG_LATITUDE,
				 * latitude); map.put(TAG_LONGITUDE, longitude);
				 * map.put(TAG_EMAIL, email); map.put(TAG_CONTACT, contact);
				 * map.put(TAG_CITY, city); map.put(TAG_STATE, state);
				 * map.put(TAG_ZIPCODE, zipcode); map.put(TAG_COUNTRY, country);
				 */
				Collection<String> values = map.values();
				values.iterator();

				detailsList.add(map);

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public class LoadShops extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(FavouriteShops.this);
			pDialog.setMessage("Fetching Data...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... arg0) {

			updateJSONdata();
			return userpassparameter;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (pDialog != null) {
					pDialog.dismiss();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (detailsList.size() == 0) {
				// AlertDialog.Builder alertDialogBuilder = new
				// AlertDialog.Builder(
				// FavouriteShops.this);
				// final SharedPreferences.Editor editor = pr.edit();
				// // set title
				// alertDialogBuilder.setTitle("");
				//
				// // set dialog message
				// alertDialogBuilder
				// .setMessage(
				// "No Shops are selected from Global Shops. Click on 'Select' button to select shops from Global Shops.")
				// .setCancelable(false)
				// .setPositiveButton("Select",
				// new DialogInterface.OnClickListener() {
				// public void onClick(DialogInterface dialog,
				// int id) {
				// // if this button is clicked, close
				//
				// Intent list = new Intent(
				// getApplicationContext(),
				// AllShopsActivity.class);
				//
				// startActivity(list);
				// }
				// });
				//
				// // create alert dialog
				// AlertDialog alertDialog = alertDialogBuilder.create();
				//
				// // show it
				// alertDialog.show();
				//
			} else {
				adapter = new ListviewAdapter(getApplicationContext(),
						detailsList);
				listview.setAdapter(adapter);
			}

		}
	}

	public class ListviewAdapter extends BaseAdapter {
		public Context mContext;

		public ListviewAdapter(Context context,
				ArrayList<HashMap<String, String>> list) {
			super();
			mContext = context;
			detailsList = list;
			for (int i = 0; i < list.size(); i++) {
				status.add(false);
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return detailsList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return detailsList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		class ViewHolder {
			TextView shopid;
			TextView shopname;
			/*
			 * TextView shopcountry; ImageView image; CheckBox checkBox;
			 */

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub

			final ViewHolder holder;
			LayoutInflater inflater = getLayoutInflater();

			if (convertView == null) {

				convertView = inflater.inflate(R.layout.favshops, null);

				holder = new ViewHolder();

				holder.shopid = (TextView) convertView
						.findViewById(R.id.fvshopid);
				holder.shopname = (TextView) convertView
						.findViewById(R.id.fvshopname);
				/*
				 * holder.checkBox = (CheckBox) convertView
				 * .findViewById(R.id.checkBox1);
				 * 
				 * holder.image = (ImageView) convertView
				 * .findViewById(R.id.imagearrow);
				 */

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			map = detailsList.get(position);
			holder.shopid.setText(map.get(TAG_ID));
			holder.shopname.setText(map.get(TAG_SHOPNAME));

			return convertView;

		}
	}

}
